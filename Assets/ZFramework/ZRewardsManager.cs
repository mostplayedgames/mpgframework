﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class ZRewardsManager : ZSingleton<ZRewardsManager>
{
    [SerializeField] List<RewardData> listOfRewardsData = new List<RewardData>();

    [Header("Rewards Data Components")]
    [SerializeField] GameObject RewardsBtn;
    [SerializeField] Image RewardIcon;
    [SerializeField] Text RewardTitle;
    [SerializeField] Text RewardDescription;

    [Space]
    [SerializeField] GameObject NoInternetConnection;
    [SerializeField] GameObject Prize;

    [Space]
    [SerializeField] RewardData CurrentRewardData;
    private RewardData tempData;

    [Space]
    [SerializeField] string CurrencyType;

    public bool canShowReward;

    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        foreach (RewardData data in listOfRewardsData)
        {
            data.Initialize();
        }
    }

    void Update()
    {
        foreach(RewardData data in listOfRewardsData)
        {
            data.RewardUpdate();
            if (data.WillShowReward())
            {
             //   Debug.Log("ShowRewards: " + data.name);
                canShowReward = true;
            }
        }
    }

    public void ShowNoInternetConnection()
    {
        NoInternetConnection.gameObject.SetActive(true);
    }

    public void ShowReward()
    {
        Debug.Log("SW1");
        foreach (RewardData data in listOfRewardsData)
        {
            if (data.WillShowReward())
            {
                tempData = data;
                InitializeButton();
                if(!data.RewardName.Equals("WATCH ADS"))
                {
                    ResetAllTimers();
                }
            }
        }
    }

    public void ClearScreen()
    {
        tempData = null;
        CurrentRewardData = null;
        RewardsBtn.gameObject.SetActive(false);
        NoInternetConnection.gameObject.SetActive(false);
        Prize.gameObject.SetActive(false);
    }

    public void ExecuteFunction()
    {
        if(ZGameMgr.instance.isOnline)
        {
            CurrentRewardData = tempData;
            CurrentRewardData.ExecuteFunction();
            RewardsBtn.gameObject.SetActive(false);
        }
        else
        {
            RewardsBtn.gameObject.SetActive(false);
            NoInternetConnection.gameObject.SetActive(true);
        }
    }

    private void InitializeButton()
    {
        RewardIcon.sprite = tempData.RewardIcon;
        RewardTitle.text = tempData.DisplayName;
        string Description = (tempData.Prize > 0) ? 
                                ("+" + tempData.Prize + " " + CurrencyType) :
                                tempData.DisplayDescription;

        RewardDescription.text = Description;
        RewardsBtn.gameObject.SetActive(true);
    }

    private void ResetAllTimers()
    {
        foreach (RewardData data in listOfRewardsData)
        {
            data.Reset();
        }
    }

    public void WatchAds(bool didAdsFinish)
    {
        Debug.Log("WatchAds: " + didAdsFinish);
        if(didAdsFinish)
        {
            if (CurrentRewardData.Prize > 0)
            {
                Debug.Log("Rewards Prize");
                ZGameMgr.instance.AddCoins(CurrentRewardData.Prize);
                Prize.gameObject.SetActive(true);
                AudioPlayerMgr.Instance.PlaySfx(AudioID.CASH_SFX);
            }
        }
    }

    private void OnApplicationFocus()
    {
        if(CurrentRewardData != null)
        {
            if(CurrentRewardData.Prize > 0)
            {
                Debug.Log("Rewards: " + CurrentRewardData.name);
                ZGameMgr.instance.AddCoins(CurrentRewardData.Prize);
                Prize.gameObject.SetActive(true);
                Prize.GetComponent<Text>().text = "+" + CurrentRewardData.Prize + " TUSOKS";
                AudioPlayerMgr.Instance.PlaySfx(AudioID.CASH_SFX);
            }

            CurrentRewardData.OnAppFocus();
            CurrentRewardData = null;
        }
    }
}
