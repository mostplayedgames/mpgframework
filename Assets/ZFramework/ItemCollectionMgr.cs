﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Will Update in the future current game behaviour
public class ItemCollectionMgr : ZSingleton<ItemCollectionMgr>
{
    public List<GameItemForks> listOfItems = new List<GameItemForks>();

    public GameItem GetItem(int p_idx)
    {
        foreach(GameItem item in listOfItems)
        {
            if(item.ItemID.Equals(p_idx))
            {
                return item;
            }
        }
        return listOfItems[0]; // If nothing matched just return the first value of the list
    }

    public void Initialize()
    {

        //if(PlayerPrefs.HasKey(""))
    }

    public void Reset()
    {

    }

    // Get Total Count of currently owned items
    public int TotalCount()
    {
        int retVal = 0;
        foreach (GameItem item in listOfItems)
        {
            //item.Initialize();
            if (item.IsItemPurchased())
            {
                retVal += 1;
            }
        }
        return retVal;
    }
}
