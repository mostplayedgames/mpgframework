﻿using UnityEngine;
using GoogleMobileAds.Api;

public class ZAdsMgr : ZSingleton<ZAdsMgr>
{
    [SerializeField] string BANNER_URL_IOS;
    [SerializeField] string BANNER_URL_ANDROID;

    [SerializeField] string INTERSTITIAL_URL_IOS;
    [SerializeField] string INTERSTITIAL_URL_ANDROID;

    BannerView bannerView;
    InterstitialAd interstitial;
    RewardBasedVideoAd m_rewardedBasedVideo;

    bool m_videoAdToReward;
    bool isInterstitialRequest;
    int m_removeAds;

    #region Properties
    public bool videoAdToReward
    {
        get { return m_videoAdToReward; }
        set { m_videoAdToReward = value; }
    }

    public int removeAds
    {
        get { return m_removeAds; }
        set { m_removeAds = value; }
    }

    public RewardBasedVideoAd rewardedBasedVideo
    {
        get { return m_rewardedBasedVideo; }
    }

    AdRequest setupAdRequest
    {
        get
        {
            return new AdRequest.Builder()
            //.TagForChildDirectedTreatment(true)
            //.AddExtra("is_designed_for_families", "true")
            //.AddExtra("tag_for_under_age_of_consent", "true")
            //.AddExtra("max_ad_content_rating", "G")
            .AddTestDevice("B4DF8A13D09D7349784BA84AF3D1F809") //Sony Xperia Z2 (Roan)
            .Build();
        }
    }
    #endregion

    void Start()
    {
        if (!PlayerPrefs.HasKey("Removeads"))
            PlayerPrefs.SetInt("Removeads", 0);

        m_removeAds = PlayerPrefs.GetInt("Removeads");
        isInterstitialRequest = false;

        //For third new, admob ID
        string _GADAppId = string.Empty;
#if UNITY_ANDROID
        _GADAppId = "ca-app-pub-2924242888617665~1627273258";
#elif UNITY_IOS
        _GADAppId = "ca-app-pub-2924242888617665~3511200561";
#endif
        Debug.Log("ZAds Mgr Start");

        MobileAds.SetiOSAppPauseOnBackground(true);
        MobileAds.Initialize(_GADAppId);

        //Init
        m_rewardedBasedVideo = RewardBasedVideoAd.Instance;
        m_rewardedBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        m_rewardedBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        RequestRewardedVideo();
        RequestInterstitial();
        ShowBanner();
    }

    public void ShowBanner()
    {
        if (m_removeAds > 0)
            return;

#if UNITY_ANDROID
        string adUnitId = BANNER_URL_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = BANNER_URL_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif

        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        bannerView.LoadAd(setupAdRequest);
    }

    public void HideBannerAd()
    {
        bannerView.Destroy();
    }

    public void RequestInterstitial()
    {
        if (m_removeAds > 0)
            return;

        if (isInterstitialRequest)
            return;

#if UNITY_ANDROID
        string adUnitId = INTERSTITIAL_URL_ANDROID;
#elif UNITY_IPHONE
        string adUnitId = INTERSTITIAL_URL_IOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        Debug.Log("Requested Intersitials");

        interstitial = new InterstitialAd(adUnitId);
        interstitial.LoadAd(setupAdRequest);
        isInterstitialRequest = true;
    }

    public void RequestRewardedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2924242888617665/8887843845";
#elif UNITY_IOS
        string adUnitId = "ca-app-pub-2924242888617665/1818844940";
#else
        string adUnitId = "unexpected_platform";
#endif

        Debug.Log("Requested Rewarded Video");

        m_rewardedBasedVideo.LoadAd(setupAdRequest, adUnitId);
    }

    public void HandleRewardBasedVideoClosed(object sender, System.EventArgs args)
    {
        if (!m_videoAdToReward)
        {
            ZRewardsManager.Instance.WatchAds(false);
            // PrototypeScene.Instance.RewardedVideoCallback(false);
            RequestRewardedVideo();
        }
        else
            m_videoAdToReward = false;
    }

    //Executes right away after finishing the rewarded video ad
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        m_videoAdToReward = true;
        ZRewardsManager.Instance.WatchAds(true);
        //PrototypeScene.Instance.RewardedVideoCallback(true);
        RequestRewardedVideo();
    }

    public bool IsInterstitialAvailable
    {
        get
        {
            if (m_removeAds > 0)
                return false;
            else if (interstitial.IsLoaded())
                return true;
            else
            {
                RequestInterstitial();
                return false;
            }
        }
    }

    public void ShowInsterstitial()
    {
        if (m_removeAds > 0)
            return;

        if (IsInterstitialAvailable)
        {
            interstitial.Show();
            isInterstitialRequest = false;
            RequestInterstitial();
        }
    }
}
