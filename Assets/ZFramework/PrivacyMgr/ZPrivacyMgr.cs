﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
	How to use?
	1. Place prefab, PrivacyPanelCanvas on the scene.
	2. Change design depending on the game's feel.
	3. PrivacyPanelParent's visibility can set to invisible when debugging. 
 */

public class ZPrivacyMgr : MonoBehaviour 
{
	[SerializeField]
	GameObject m_parentObject;

	[SerializeField]
	Button m_OkButton;
	const string m_PrivacyPref = "PrivacyOk";

	void Awake()
	{
	//	if (PlayerPrefs.HasKey(m_PrivacyPref))
	//	{
	//		SceneManager.LoadScene(1);
	//		gameObject.SetActive(false);
	//	}
	//	else
	//	{
			m_OkButton.interactable = false;
	//		Time.timeScale = 0;
	//		m_parentObject.SetActive(true);
	//	}
	}

	public void okToggleChange(Toggle _checkToggle)
	{
		m_OkButton.interactable = _checkToggle.isOn;
	}

	public void buttonCallback(int _getIndex)
	{
		switch (_getIndex)
		{
			case 0: //Ok
				gameObject.SetActive(false);
				PlayerPrefs.SetInt(m_PrivacyPref, 1);
				//Time.timeScale = 1;
				//SceneManager.LoadScene(1);
			break;
			case 1: //Privacy Policy
				Application.OpenURL("https://mostplayedgames.co/privacy-policy/");
			break;
		}
	}
}
