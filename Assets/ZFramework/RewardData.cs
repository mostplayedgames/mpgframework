﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Advertisements;

public enum REWARD_TYPE
{
    OPEN_LINK,
    OPEN_SHOP,
    WATCH_ADS
}

[CreateAssetMenu(fileName = "GameItem", menuName = "RewardItem/BaseReward", order = 1)]
public class RewardData : ScriptableObject
{
    [Header("General Reward Data")]
    public string RewardName;
    public string DisplayName;
    public string DisplayDescription;
    public Sprite RewardIcon;
    public int Prize;
    public REWARD_TYPE RewardType;

    [Header("Timer Related Varibles")]
    public int TimeInitial;
    public int TimeRecurring;
    public int AdditiveTime;

    [Header("Colors")]
    public Color FirstColor;
    public Color SecondColor;

    [Header("Game Links")]
    [SerializeField] string androidLink;
    [SerializeField] string iosLink;

    [Space]
    public bool isConsumeable;

    public float Timer;

    public virtual void RewardUpdate()
    {
        Timer -= Time.deltaTime;
    }

    public virtual void Initialize()
    {
        if(!PlayerPrefs.HasKey("" + DisplayName))
        {
            PlayerPrefs.SetInt("" + DisplayName, 0);
        }
        Timer = TimeInitial;

    }

    public bool WillShowReward()
    {
        // Check if timer is 0 and if 
        bool retVal = (Timer <= 0 && PlayerPrefs.GetInt("" + DisplayName) <= 0) ? true : false;

        if(RewardType == REWARD_TYPE.WATCH_ADS)
        {
            return ZAdsMgr.Instance.rewardedBasedVideo.IsLoaded() && retVal;
        }
        else
        {
            return retVal;
        }
    }

    public virtual void Reset()
    {
        bool DidShowReward = WillShowReward();
        Debug.Log("DidShowReward: " + DidShowReward + "_" + RewardName);
        Timer = TimeRecurring;
        Timer += DidShowReward ? AdditiveTime : 0;
    }

    public virtual void ExecuteFunction()
    {
        switch(RewardType)
        {
            case REWARD_TYPE.OPEN_LINK:
                {
                    Debug.Log("OpenLink");
#if UNITY_ANDROID
                Application.OpenURL(androidLink);
#elif UNITY_IOS
                Application.OpenURL(iosLink);
#endif
                }
                break;
            case REWARD_TYPE.WATCH_ADS:
                {
                    if(ZAdsMgr.Instance.rewardedBasedVideo.IsLoaded())
                    {
                        ZAdsMgr.Instance.rewardedBasedVideo.Show();
                    }
                    //ZAdsMgr.Instance.ShowRewardedVideo(AdCallbackHandler);
                }
                break;
        }
    }

    void AdCallbackHandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("Video Finished");
                break;
            case ShowResult.Skipped:
                Debug.Log("Video Skipped");
                break;
            case ShowResult.Failed:
                Debug.Log("Video Failed");
                break;
        }
    }


    public virtual void OnAppFocus()
    {
        // If reward should only appear one time
        if (isConsumeable)
        {
            PlayerPrefs.SetInt("" + DisplayName, 1);
        }
    }
}
