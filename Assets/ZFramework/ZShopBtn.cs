﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZShopBtn : MonoBehaviour
{
    [SerializeField] GameItem ItemData;
    [SerializeField] Image ItemImage;
    [SerializeField] GameObject QuestionMarkText;

    [SerializeField] Image BtnBackground;

    [SerializeField] GameObject InUseTag;

    [Header("Sprite Assets")]
    [SerializeField] Sprite LockedSpr;
    [SerializeField] Sprite UnlockedSpr;

    [Header("Colors")]
    [SerializeField] Color Locked;
    [SerializeField] Color Unlocked;
    //[SerializeField] Color Selected;

    public virtual void Initialize(GameItem gameItem, bool isSelected = false)
    {
        ItemData = gameItem;
        //ItemData.Initialize();
        bool isPurchased = gameItem.IsItemPurchased();
        ItemImage.sprite = gameItem.ItemShopImage;

        if (isPurchased)
        {
            QuestionMarkText.gameObject.SetActive(false);
            ItemImage.gameObject.SetActive(true);
            BtnBackground.color = Unlocked;
            BtnBackground.sprite = (isSelected) ? UnlockedSpr: LockedSpr;

            InUseTag.gameObject.SetActive(isSelected);
            //BtnBackground.sprite = (isSelected) ? LockedItem : UnlockedItem;
            //BtnBackground.color = (isSelected) ? Selected : Unlocked;
            // ShopManager.Instance.Initialize(ItemData.ItemID);
        }
        else
        {
            QuestionMarkText.gameObject.SetActive(true);
            ItemImage.gameObject.SetActive(false); 
            BtnBackground.color = Locked;
            BtnBackground.sprite = LockedSpr;
            InUseTag.gameObject.SetActive(false);
        }
    }

    public GameItem GetData()
    {
        return ItemData;
    }

    public void PurchaseItem()
    {
        ItemData.PurchaseItem(0);
        MTSFork.Instance.SetCharacter(ItemData, false);
        PlayerPrefs.SetInt("ItemSelected", ItemData.ItemID);
        //ShopManager.Instance.Initialize(ItemData.ItemID);
    }

    public void Activate()
    {
        if (ItemData.IsItemPurchased())
        {
            MTSFork.Instance.SetCharacter(ItemData, true);
            ShopManager.Instance.Initialize(ItemData.ItemID);
        }

    }
}
