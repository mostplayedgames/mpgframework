﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZAnimateButtonColor : MonoBehaviour
{
    [SerializeField] Color baseColor;
    [SerializeField] Color highlightColor;
    // Start is called before the first frame update
    void OnEnable()
    {
        this.GetComponent<Image>().color = baseColor;
        StopCoroutine("AnimateRewardsButton");
        StartCoroutine("AnimateRewardsButton");
    }

    IEnumerator AnimateRewardsButton()
    {
        while (true)
        {
            this.GetComponent<Image>().color = baseColor;
            yield return new WaitForSeconds(0.5f);

            this.GetComponent<Image>().color = highlightColor;
            yield return new WaitForSeconds(0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
