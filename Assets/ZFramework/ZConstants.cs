﻿using UnityEngine;
using System.Collections;

public class ZConstants : MonoBehaviour {

	public static string ADMOB_URL_IOS = "";
	public static string ADMOB_URL_ANDROID = "";

	public static string CHARTBOOST_URL_IOS = "";
	public static string CHARTBOOST_URL_ANDROID = "";

	public static string RATEUS_URL_IOS = "";
	public static string RATEUS_URL_ANDROID = "https://play.google.com/store/apps/details?id=com.mostplayed.holdcharge";

}
