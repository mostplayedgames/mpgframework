﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomShopUnlockMgr : ZSingleton<RandomShopUnlockMgr>
{
    [SerializeField] MTSGameManager gameManager;
    [SerializeField] GameObject loadingBar;
    [SerializeField] GameObject slidingBar;

    [Header("Unlock Sequence1")]
    [SerializeField] GameObject unlockButton;
    [SerializeField] Text UnlockBtnText;

    [Header("Unlock Sequence2")]
    [SerializeField] GameObject ContinueBotGroup;
    [SerializeField] ParticleSystem FlashParticles;
    [SerializeField] Text ContinueItemText;


    [SerializeField] List<ZShopBtn> listOfShopBtns = new List<ZShopBtn>();

    public int currentCount;
    public int totalCount;
    public float limit;
    public float timer;
    public int toUnlock;
    public bool unlocking;

    public void Start()
    {
        unlocking = false;
    }


    public void NewCharacterToUnlock()
    {
        StopCoroutine("UnlockSequence1");
        StartCoroutine("UnlockSequence1");
    }

    public IEnumerator UnlockSequence1()
    {
        // 1 Scale Loading bar 
        LeanTween.cancel(loadingBar.gameObject);
        LeanTween.scale(loadingBar.gameObject, loadingBar.transform.localScale + new Vector3(0.1f, 0.1f, 0.1f), 0.1f);//.setLoopPingPong().setLoopCount(2);
        yield return new WaitForSeconds(0.1f);
        AudioPlayerMgr.Instance.PlaySfx(AudioID.BONUS_SFX);
        LeanTween.scale(loadingBar.gameObject, loadingBar.transform.localScale - new Vector3(0.1f, 0.1f, 0.1f), 0.1f);

        yield return new WaitForSeconds(1f);

        // 2 Slide Brown For Unlocking
        slidingBar.transform.localPosition = new Vector3(1300, 0, 0);
        LeanTween.cancel(slidingBar.gameObject);
        LeanTween.moveLocalX(slidingBar.gameObject, -1300, 1.5f);
        AudioPlayerMgr.Instance.PlaySfx(AudioID.SWOOSH_SFX);
        yield return new WaitForSeconds(0.65f);
        gameManager.ShowTitleScreen(false);
        UnlockBtnText.text = "" + gameManager.GetTargetCount() + " TUSOKS"; 
        gameManager.UnlockPreview();
        unlockButton.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);
    }

    public void FlickerUnlock()
    {
        limit = 0.1f;
        timer = 0;
        totalCount = 0;
        currentCount = Random.Range(0, ItemCollectionMgr.Instance.listOfItems.Count);
        unlocking = true;
        listOfShopBtns = ShopManager.Instance.GetItemsToUnlock();
        toUnlock = Random.Range(0, listOfShopBtns.Count);
        startUnlock = false;
    }

    public void Update()
    {
        //if(Input.GetKeyDown(KeyCode.M))
        //{
        //    limit = 0.1f;
        //    timer = 0;
        //    totalCount = 0;
        //    currentCount = Random.Range(0, ItemCollectionMgr.Instance.listOfItems.Count);
        //    toUnlock = Random.Range(0, listOfShopBtns.Count);
        //    unlocking = true;
        //    listOfShopBtns = ShopManager.Instance.GetItemsToUnlock();
        //}

        if(Input.GetKeyDown(KeyCode.N))
        {
            NewCharacterToUnlock();
        }


        if (unlocking)
        {
            timer += Time.deltaTime;

            if (timer > limit)
            {
                AudioPlayerMgr.Instance.PlaySfx(AudioID.RANDOM_TICK_SFX);
                UpdateCount();
                timer = 0;
                if (totalCount > 15)
                {
                    limit += 0.01f + (limit * 0.3f);// * Time.deltaTime;
                }
            }

            if(totalCount >= 24)
            {
                unlocking = false;
                MTSFork.Instance.SetShopPreview(ItemCollectionMgr.Instance.GetItem(listOfShopBtns[toUnlock].GetData().ItemID));
                listOfShopBtns[toUnlock].PurchaseItem();
                StopCoroutine("UnlockSequence2");
                StartCoroutine("UnlockSequence2");
            }
        }
    }

    public IEnumerator UnlockSequence2()
    {
        GameObject hand = MTSFork.Instance.gameObject;
        LeanTween.cancel(hand.gameObject);
        LeanTween.scale(hand.gameObject, hand.transform.localScale + new Vector3(0.2f, 0.2f, 0.2f), 0.1f).setEase(LeanTweenType.easeOutExpo);
        yield return new WaitForSeconds(0.1f);
        FlashParticles.gameObject.SetActive(true);
        FlashParticles.Stop();
        FlashParticles.Play();
        LeanTween.scale(hand.gameObject, hand.transform.localScale - new Vector3(0.2f, 0.2f, 0.2f), 0.1f).setEase(LeanTweenType.easeOutExpo);
        yield return new WaitForSeconds(0.1f);
        AudioPlayerMgr.Instance.PlaySfx(AudioID.UNLOCK_SFX);
        ContinueItemText.gameObject.SetActive(true);
        ContinueItemText.transform.localScale = Vector3.zero;
        LeanTween.cancel(ContinueItemText.gameObject);
        LeanTween.scale(ContinueItemText.gameObject, new Vector3(0.36f, 0.36f, 0.36f), 0.25f);
        ContinueItemText.text = "UNLOCKED\n" + ItemCollectionMgr.Instance.GetItem(listOfShopBtns[toUnlock].GetData().ItemID).ItemName;
        yield return new WaitForSeconds(2f);
        AudioPlayerMgr.Instance.PlaySfx(AudioID.SHORT_NOTIF_SFX);
        ContinueBotGroup.gameObject.SetActive(true);
        //ContinueBotGroup.transform.localPosition = new Vector3(0, 0, 0);
        //LeanTween.cancel(ContinueBotGroup.gameObject);
        //LeanTween.moveLocalY(ContinueBotGroup, 200, 0.5f).setEase(LeanTweenType.easeInOutSine);
    }

    public void ContinueFromUnlock()
    {
        ContinueBotGroup.gameObject.SetActive(false);
        ContinueItemText.gameObject.SetActive(false);
        ShopManager.Instance.Initialize();
    }

    public void Unlock()
    {
        startUnlock = true;
        //int totalCount = gameManager.GetTotalCount();
        //int targetCount = gameManager.GetTargetCount();
        //loadingBar.GetComponent<ZLoadingBar>().UpdateBar(totalCount, targetCount);
        unlockButton.gameObject.SetActive(false);
        AudioPlayerMgr.Instance.PlaySfx(AudioID.CASH_SFX);
        gameManager.Purchase();
    }

    bool startUnlock;
    private void UpdateCount()
    {
        if(startUnlock)
        { 
            totalCount += 1;
        }

        if (totalCount >= 23)
        {
            MTSFork.Instance.SetShopPreview(ItemCollectionMgr.Instance.GetItem(listOfShopBtns[toUnlock].GetData().ItemID));
        }
        else
        {
            currentCount = (int)Mathf.Repeat(currentCount + 1, ItemCollectionMgr.Instance.listOfItems.Count);
            MTSFork.Instance.SetShopPreview(ItemCollectionMgr.Instance.GetItem(currentCount));
        }
    }
}
