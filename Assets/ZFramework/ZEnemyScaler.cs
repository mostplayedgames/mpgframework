﻿using UnityEngine;
using System.Collections;

public class ZEnemyScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LeanTween.scaleY (this.gameObject, 1.85f, 0.5f).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
