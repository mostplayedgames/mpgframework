﻿using UnityEngine;
using System.Collections;

public class ZScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LeanTween.scale (this.gameObject, this.gameObject.transform.localScale + new Vector3 (0.02f, 0.02f, 0.02f), 0.7f).setLoopPingPong ().setEase(LeanTweenType.easeInOutQuad);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
