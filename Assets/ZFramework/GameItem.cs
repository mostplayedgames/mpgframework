﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "GameItem", menuName = "GameItem/Item", order = 1)]
public class GameItem : ScriptableObject
{
    [Header("General Information")]
    public int ItemID;                  // ID;
    public string ItemName;             // Name of the Item
    public string ItemDescription;      // Description of the item  
    public Sprite ItemImage;            // The Actual image of the item in the game
    public Sprite ItemShopImage;        // The image of the item in the shop screen
    public int ItemPrice;               // Price of the item
    public bool IsItemPurchased()
    {
        if (!PlayerPrefs.HasKey("Bought_" + ItemName))
        {
            int saveFile = (IsDefaultItem) ? 1 : 0;
            PlayerPrefs.SetInt("Bought_" + ItemName, saveFile);
        }

       return (PlayerPrefs.GetInt("Bought_" + ItemName) > 0) ? true : false;

    }

    [Space]
    public bool IsDefaultItem;

    //public void Initialize()
    //{
    //    if(!PlayerPrefs.HasKey("Bought_" + ItemName))
    //    {
    //        int saveFile = (IsDefaultItem) ? 1 : 0;
    //        PlayerPrefs.SetInt("Bought_" + ItemName, saveFile);
    //    }
    //    else
    //    {
    //        IsItemPurchased = (PlayerPrefs.GetInt("Bought_" + ItemName) > 0) ? true: false;
    //    }
    //}

    public void Reset()
    {
        PlayerPrefs.SetInt("Bought_" + ItemName, 0);
    }

    public bool PurchaseItem(int p_coin)
    {
        if(ItemPrice >= p_coin)
        {
            //IsItemPurchased = true;
            PlayerPrefs.SetInt("Bought_" + ItemName, 1);
            return true;
        }
        return false;
    }
}