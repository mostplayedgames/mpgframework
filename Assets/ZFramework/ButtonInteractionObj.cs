﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInteractionObj : MonoBehaviour 
{   
	[SerializeField]
	Button m_ButtonComponent;

	[SerializeField]
	Image m_ButtonImg;
	Vector2 m_normalParentPos;

	[Space]
	[SerializeField]
    RectTransform m_ParentObjRect;

	[SerializeField]
	Shadow m_Shadow;

	[SerializeField]
	float m_ParentPressedPos = -6.75f;

	void Awake()
	{
		m_normalParentPos = m_ParentObjRect.anchoredPosition;
	}

	void Update()
	{
		bool _isPressed = m_ButtonImg.overrideSprite == m_ButtonComponent.spriteState.pressedSprite;
		m_ParentObjRect.anchoredPosition = new Vector2(m_normalParentPos.x,
		                                               m_normalParentPos.y + (_isPressed ? m_ParentPressedPos : 0));

		if (m_Shadow != null)
		    m_Shadow.enabled = !_isPressed;
	}
}
