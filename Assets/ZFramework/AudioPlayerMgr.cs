﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioID
{
    BUTTON_SFX,
    SHANGHAI_SFX,
    SWOOSH_SFX,
    ERROR_SFX,
    BONUS_SFX,
    QUICKCHARGE_SFX,
    UNLOCK_SFX,
    RANDOM_TICK_SFX,
    CASH_SFX,
    NOTIFICATION_SFX,
    SHORT_NOTIF_SFX
}

public class AudioPlayerMgr : ZSingleton<AudioPlayerMgr>
{
    [SerializeField] AudioClip audioBtn;
    [SerializeField] AudioClip audioShanghaiSfx;
    [SerializeField] AudioClip audioSwooshSfx;
    [SerializeField] AudioClip audioError;
    [SerializeField] AudioClip audioBonus;
    [SerializeField] AudioClip audioQuickCharge;
    [SerializeField] AudioClip audioUnlockSfx;
    [SerializeField] AudioClip audioRandomTickSfx;
    [SerializeField] AudioClip audioCashSfx;
    [SerializeField] AudioClip audioNotificationSfx;
    [SerializeField] AudioClip audioShortNotification;

    public void PlaySfx(AudioID p_id)
    { 
        switch (p_id)
        {
            case AudioID.BUTTON_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioBtn);
                }
                break;
            case AudioID.SHANGHAI_SFX:
                {
                    float start = Random.Range(1, 4);
                    ZAudioMgr.Instance.PlaySoundInterval(audioShanghaiSfx, start, start + 0.5f);
                }
                break;
            case AudioID.SWOOSH_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioSwooshSfx);
                }
                break;
            case AudioID.ERROR_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioError);
                }
                break;
            case AudioID.BONUS_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioBonus);
                }
                break;
            case AudioID.QUICKCHARGE_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioQuickCharge);
                }
                break;
            case AudioID.CASH_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioCashSfx);
                }
                break;
            case AudioID.UNLOCK_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioUnlockSfx);
                }
                break;
            case AudioID.RANDOM_TICK_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioRandomTickSfx,false, Random.Range(0.8f, 1f));
                }
                break;
            case AudioID.NOTIFICATION_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioNotificationSfx);
                }
                break;
            case AudioID.SHORT_NOTIF_SFX:
                {
                    ZAudioMgr.Instance.PlaySFX(audioShortNotification);
                }
                break;
        }
    }
}


