﻿using UnityEngine;
using System.Collections;

public class ZCameraMgr : MonoBehaviour {

	public static ZCameraMgr instance;

	public bool Shaking; 
	private float ShakeDecay;
	private float ShakeIntensity;    
	private Vector3 OriginalPos;
	private Quaternion OriginalRot;

	void Start()
	{
		instance = this;
		Shaking = false;   
	}

	void Update () 
	{
        if (ShakeIntensity > 0)
		{
			transform.position = OriginalPos + Random.insideUnitSphere * ShakeIntensity;
			transform.rotation = new Quaternion(OriginalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.z + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f);

			ShakeIntensity -= ShakeDecay;
		}
		else if (Shaking)
		{
			Shaking = false;  
			transform.position = OriginalPos;
			transform.rotation = OriginalRot;
		}
	}

    public void VerticalShake(float p_intensity)
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalY(this.gameObject, this.transform.localPosition.y - p_intensity, 0.1f).setLoopPingPong().setLoopCount(2);
    }

    public void DoShake()
	{
		OriginalPos = transform.position;
		OriginalRot = transform.rotation;

		ShakeIntensity = 0.1f;
		ShakeDecay = 0.01f;
		Shaking = true;
	}
}


