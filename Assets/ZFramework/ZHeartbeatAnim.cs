﻿using UnityEngine;
using System.Collections;

public class ZHeartbeatAnim : MonoBehaviour 
{
    public float scaleUp = 1.3f;
    public float defaultScale = 1;
    public float timeScale = 0.2f;
    public float timeHeartbeat = 2f;
    bool m_isInit;
     
    void Start () 
    {
        NormalizeTween();
        m_isInit = true;
    }

    void OnEnable()
    {
        if (m_isInit)
            NormalizeTween();
    }

    void OnDisable()
    {
        transform.localScale = new Vector3(defaultScale, defaultScale, defaultScale);
        LeanTween.cancel(gameObject);
    }

    void NormalizeTween()
    {
        LeanTween.cancel(gameObject);
        transform.localScale = new Vector3(defaultScale, defaultScale, defaultScale);
        Heartbeat();
    }   

    void Heartbeat()
    {
        LeanTween.scale (gameObject, new Vector3(scaleUp,scaleUp,scaleUp), timeScale).setOnComplete (HeartbeatDown);
    }

    void HeartbeatDown()
    {
        LeanTween.scale (gameObject, new Vector3(defaultScale,defaultScale,defaultScale), timeScale).setOnComplete (Wait);
    }

    void Wait()
    {
        LeanTween.delayedCall (timeHeartbeat, Heartbeat);
    }
}
