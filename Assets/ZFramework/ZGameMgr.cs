﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.IO;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public enum ZGameState
{
    MAIN_MENU,
    IN_GAME,
    GAME_OVER,
    SHOP
}

public class ZGameMgr : MonoBehaviour {

	public static ZGameMgr instance;

	public string INSTANT_NOTIFICATION_MESSAGE;
	public string NOTIFICATION_MESSAGE;
	public int NOTIFICATION_TIME;

    int m_currentAds;

    public bool isOnline = false;

    public ZGameState curState;
    public ZGameMode curGameMode;

    [SerializeField] GameObject PrivacyPolicy;

    [Space]
    public UnityEvent OnInitialize; 

    [Space]
    public UnityEvent OnGameOver;

	// Use this for initialization
	void Awake () 
	{
		instance = this;
		Application.targetFrameRate = 60;

        m_currentAds = 0;
        curState = ZGameState.MAIN_MENU;
        ZAudioMgr.Instance.enabled = true;
        //ZAdsMgr.Instance.enabled = true;
        ZIAPMgr.Instance.enabled = true;
        ZPlatformCenterMgr.Instance.enabled = true;
        ZNotificationMgr.Instance.enabled = true;
        ZAdsMgr.Instance.RequestInterstitial();

        iPhoneXResolutionFix();

        if(!PlayerPrefs.HasKey("PrivacyOk"))
        {
            PlayerPrefs.SetInt("PrivacyOk", 0);
        }

        if(PlayerPrefs.GetInt("PrivacyOk") < 1)
        {
            PrivacyPolicy.gameObject.SetActive(true);
        }
    }

    void Start() 
	{
		RefreshInternet ();
		ZNotificationMgr.Instance.ClearNotifications ();
        ZPlatformCenterMgr.Instance.Login();
    }




    [HideInInspector]
    public bool m_hasNotch;

    [HideInInspector]
    public float m_NotchOffset = 20f;

    [SerializeField]
    RectTransform[] m_iPhoneXResolutionAffects;

    public void iPhoneXResolutionFix()
    {
        //Checks if iPhoneX
#if UNITY_IOS
        if (Device.generation >= DeviceGeneration.iPhoneX)
        {
            foreach (RectTransform _t in m_iPhoneXResolutionAffects)
            {
                try
                {
                    Vector2 _anchoredPos = _t.anchoredPosition;
                    _anchoredPos.y -= m_NotchOffset;
                    _t.anchoredPosition = _anchoredPos;
                }
                catch //because sometimes we tend to forget things
                {
                    continue;
                }
            }

            m_hasNotch = true;
        }
#endif
    }

	void Update () 
	{
		switch(curState)
        {
            case ZGameState.MAIN_MENU:
                if(!IsPointerOverUIObject && Input.GetMouseButtonDown(0))
                {
                    curGameMode.ZInitialize();
                    OnInitialize.Invoke();
                    curState = ZGameState.IN_GAME;
                }
                break;
            case ZGameState.IN_GAME:
                curGameMode.ZUpdate();
                break;
            case ZGameState.GAME_OVER:
                break;
            case ZGameState.SHOP:
                break;
        }
    }
 

    public void Score(int p_score)
    {
        curGameMode.ZScore(p_score);
    }

    public int GetScore()
    {
        return curGameMode.ZGetScore();
    }

    public void GameOver()
    {
        curState = ZGameState.GAME_OVER;
        curGameMode.ZGameOver();
        OnGameOver.Invoke();
        m_currentAds++;
    }

    public bool WillShowRewards()
    {
        int highscore = curGameMode.ZGetHighScore();
        if (highscore > 5 && m_currentAds > 5 && ZAdsMgr.Instance.IsInterstitialAvailable)
        {
            return true;
        }
        else if (highscore > 2 && ZRewardsManager.Instance.canShowReward)
        {
            return true;
        }
        return false;
    }

    public void ShowRewards()
    {
        int highscore = curGameMode.ZGetHighScore();

        int score = curGameMode.ZGetHighScore();

#if UNITY_ANDROID
        string leaderboardId = curGameMode.leaderboardData.leaderboardAndroid;
        ZPlatformCenterMgr.Instance.PostScore(score, leaderboardId);
#elif UNITY_IOS
        string leaderboardId = curGameMode.leaderboardData.leaderboardIOS;
        ZPlatformCenterMgr.Instance.PostScore(score, leaderboardId);
#endif

        if (highscore > 5 && m_currentAds > 5 && ZAdsMgr.Instance.IsInterstitialAvailable)
        {
            Debug.Log("ShowInterstial");
            ZAdsMgr.Instance.ShowInsterstitial();
            m_currentAds = 0;
        }
        else if (highscore > 2 && ZRewardsManager.Instance.canShowReward)
        {
            Debug.Log("ShowRewards");
            ZRewardsManager.Instance.ShowReward();
        }
    }

    void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus) 
        {
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.INSTANT_NOTIFICATION_MESSAGE, 120);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 4);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 12);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 20);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 28);
			ZNotificationMgr.Instance.AddNotification (ZGameMgr.instance.NOTIFICATION_MESSAGE, ZGameMgr.instance.NOTIFICATION_TIME * 34);
		} 
        else 
        {
			ZNotificationMgr.Instance.ClearNotifications ();
		} 
	}

    public void AddCoins(int p_coins)
    {
        curGameMode.ZAddCoins(p_coins);
    }

    public void OpenLeaderboard()
    {
        AudioPlayerMgr.Instance.PlaySfx(AudioID.BUTTON_SFX);
#if UNITY_ANDROID
        ZPlatformCenterMgr.Instance.ShowLeaderboardUI(curGameMode.leaderboardData.leaderboardAndroid);
#elif UNITY_IOS
        ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
#endif
    }

    public void RefreshInternet()
	{
		//StartCoroutine ("CheckInternet");
		CheckInternetReachability();
	}

    public void SharePhoto ()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE))
            ZFollowUsMgr.Instance.Share();
        else 
            UniAndroidPermission.RequestPermission (AndroidPermission.WRITE_EXTERNAL_STORAGE, SharePhoto);
#else
        ZFollowUsMgr.Instance.Share();
#endif
    }

    void CheckInternetReachability(){
		switch (Application.internetReachability) {
		case NetworkReachability.NotReachable:
			isOnline = false;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			isOnline = true;
			break;
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			isOnline = true;
			break;
		default :
			isOnline = false;
			break;
		}
	}

	IEnumerator CheckInternet()
	{
		string HtmlText = GetHtmlFromUri("http://google.com");
		if(HtmlText == "")
		{
			isOnline = false;
		}
		else if(!HtmlText.Contains("schema.org/WebPage"))
		{
			isOnline = false;
		}
		else
		{
			isOnline = true;
		}

		return null;
	}

	string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
		req.Timeout = 5000;
		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
					{
						//We are limiting the array to 80 so we don't have
						//to parse the entire html document feel free to 
						//adjust (probably stay under 300)
						char[] cs = new char[80];
						reader.Read(cs, 0, cs.Length);
						foreach(char ch in cs)
						{
							html +=ch;
						}
					}
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}

    public bool IsPointerOverUIObject
    {
        get
        {
            PointerEventData _eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            _eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> _results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(_eventDataCurrentPosition, _results);
            return _results.Count > 0;
        }
    }
}
