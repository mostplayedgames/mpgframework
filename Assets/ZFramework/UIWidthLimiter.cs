﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UIWidthLimiter : MonoBehaviour 
{
	[SerializeField]
	RectTransform m_CanvasReference;

	[SerializeField]
	RectTransform m_RectTransform;

	[SerializeField]
	float m_widthLimit;

	[SerializeField]
	float m_Margins;

	#region Properties
	public float widthLimit
	{
		set { m_widthLimit = value; }
	}
	#endregion

	void Start()
	{
		setSizeFix();
	}

	#if UNITY_EDITOR
	void Update()
	{
		if (m_CanvasReference == null)
			return;

		if (m_RectTransform == null)
			return;

		setSizeFix();
	}
	#endif

	void setSizeFix()
	{
		m_RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Clamp(m_CanvasReference.rect.width - m_Margins, Mathf.NegativeInfinity, m_widthLimit));
	}
}
