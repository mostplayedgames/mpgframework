﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZGameMode : MonoBehaviour
{
    [SerializeField] public LeaderboardData leaderboardData;
    public virtual void ZInitialize()
    {

    }

    public virtual void ZUpdate()
    {

    }

    public virtual void ZGameOver()
    {

    }

    public virtual void ZScore(int p_score)
    {

    }

    public virtual void ZAddCoins(int p_addCoins)
    {

    }

    public virtual int ZGetHighScore()
    {
        return 0;
    }

    public virtual int ZGetScore()
    {
        return 0;
    }
}
