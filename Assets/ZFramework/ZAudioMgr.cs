﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZAudioMgr : ZSingleton<ZAudioMgr> {

	List<AudioSource> m_listAudioSource;

	void Awake()
	{
		InstantiateAudioSources ();
	}

	void InstantiateAudioSources()
	{
		m_listAudioSource = new List<AudioSource> ();

		for(int x = 0; x < 16; x++) {
			m_listAudioSource.Add(this.gameObject.AddComponent<AudioSource>());
		}
	}

	AudioSource GetAvailableAudioSource()
	{
		foreach (AudioSource source in m_listAudioSource) {
			if (!source.isPlaying) {
				return source;
			}
		}
		return null;
	}

	public void StopSFX(AudioClip clip)
	{
		for(int x = 0; x < 16; x++) {
			if( m_listAudioSource[x].clip == clip )
			{	m_listAudioSource[x].Stop();
			}
		}
	}

	public void PlaySFX(AudioClip clip, bool looping, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = looping;
		source.clip = clip;
		source.Play ();
        source.SetScheduledEndTime(AudioSettings.dspTime);
    }

    public void PlaySFX(AudioClip clip, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();
		
		source.loop = looping;
		source.clip = clip;
		source.volume = 1.0f;
		source.Play ();
        source.SetScheduledEndTime(AudioSettings.dspTime);
    }

    public void PlaySoundInterval(AudioClip clip, float fromSeconds, float toSeconds)
    {
        AudioSource source = GetAvailableAudioSource();

        source.clip = clip;
        source.time = fromSeconds;
        source.Play();
        source.SetScheduledEndTime(AudioSettings.dspTime + (toSeconds - fromSeconds));
    }

    public void PlaySFX(AudioClip clip)
	{
		AudioSource source = GetAvailableAudioSource ();

		if (source) {
			source.loop = false;
			source.clip = clip;
			source.volume = 1.0f;
			source.Play ();
            source.pitch = 1;
		}
	}

	public void PlaySFXVolume(AudioClip clip, float volume)
	{
		AudioSource source = GetAvailableAudioSource ();

		if (source) {
			source.loop = false;
			source.clip = clip;
			source.volume = volume;
			source.Play ();
            source.pitch = 1;
        }
    }

	public void PlaySFX(AudioClip clip, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = false;
		source.clip = clip;
		source.volume = 1.0f;
		source.Play ();
	}
}
