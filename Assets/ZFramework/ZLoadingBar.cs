﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZLoadingBar : MonoBehaviour
{
    [SerializeField] Image BackBar;
    [SerializeField] Image FrontBar;
    [SerializeField] Text TextBar;

    public void UpdateBar(float p_value, float p_target)
    {
        Debug.Log("BarValue: " + p_value);
        float fill = Mathf.Min(p_value / p_target, 1);
        FrontBar.fillAmount = fill;
        TextBar.text = "" + p_value + "/" + p_target + " TUSOKS";
    }

    public void AnimateTo()
    {

    }

    public void Reset()
    {
        FrontBar.fillAmount = 0;
    }
}
