﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PoolObjType
{
    SHANGHAI,
    SHANGHAI_PARTICLES,
    UI_EXPRESSION
}

[System.Serializable]
public class PoolObj
{
    public string ObjID;
    public GameObject ObjToPool;
}

public class PooledObjects : ZSingleton<PooledObjects>
{
    [SerializeField] List<PoolObj> listOfPooledObj = new List<PoolObj>();

    [SerializeField] GameObject ShanghaiObj;
    [SerializeField] GameObject ShanghaiParticles;

    [SerializeField] GameObject ExpressionText;
    [SerializeField] GameObject ExpressionTextParent;

    private void Awake()
    {
        ZObjectMgr.Instance.AddNewObject(ShanghaiObj, 30,"ShanghaiObj");
        ZObjectMgr.Instance.AddNewObject(ShanghaiParticles, 10, "LumpiaParticles");
        ZObjectMgr.Instance.AddNewObject(ExpressionText, 10, "UIExpression");
    }

    public GameObject GetObj(PoolObjType p_objType)
    {
        switch(p_objType)
        {
            case PoolObjType.SHANGHAI:
                {
                    return ZObjectMgr.Instance.Spawn3D("ShanghaiObj", Vector3.zero);
                }
                break;
            case PoolObjType.SHANGHAI_PARTICLES: 
                {
                    GameObject obj = ZObjectMgr.Instance.Spawn3D("LumpiaParticles", Vector3.zero);
                    obj.GetComponent<ParticleSystem>().Stop();
                    obj.GetComponent<ParticleSystem>().Play();
                    return obj;
                }
                break;
            case PoolObjType.UI_EXPRESSION:
                {
                    GameObject obj = ZObjectMgr.Instance.Spawn3D("UIExpression", Vector3.zero);
                    obj.transform.parent = ExpressionTextParent.transform;
                    return obj;
                }
            default:
                {
                    return ZObjectMgr.Instance.Spawn2D("ShanghaiObj", Vector3.zero);
                }
                break;
        }
    }
}
