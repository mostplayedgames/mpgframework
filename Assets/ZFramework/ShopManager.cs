﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopManager : ZSingleton<ShopManager>
{
    [SerializeField] ItemCollectionMgr ItemCollection;
    [SerializeField] List<ZShopBtn> listOfShopBtns = new List<ZShopBtn>();

    [Header("Shop Data")]
    [SerializeField] GameObject ShopGroup;
    [SerializeField] float YMax = 0;
    [SerializeField] float YMin = -800;
    [SerializeField] float AnimationSpeed;

    [Header ("Grid Fix in Game")]
    [SerializeField]
    RectTransform m_ItemsRectBasis;

    [SerializeField]
    RectTransform m_ItemsGrid;

    [SerializeField]
    Vector2 m_itemRectLimit;

    [SerializeField]
    GridLayoutGroup m_ItemsGridLayoutGroup;


    public UnityEvent WhenInitialized;
    public UnityEvent WhenInitializedOnce;
    public UnityEvent WhenDestroyed;
    public bool isInitialized;

    public int curItemSelected = 0;

    public void Awake()
    {
        GridConstraintFix();
        int idx = 0;
        Debug.Log("Shop");
        foreach (ZShopBtn btn in listOfShopBtns)
        {
            GameItem data = ItemCollection.GetItem(idx);
            bool isSelected = (idx == curItemSelected) ? (true) : (false);
            btn.Initialize(data, isSelected);
            idx++;
        }
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            Reset();
            //Initialize();
        }
        else if(Input.GetKeyDown(KeyCode.W))
        {
            Close();
        }
    }

    public List<ZShopBtn> GetItemsToUnlock()
    {
        List<ZShopBtn> retVal = new List<ZShopBtn>();
        foreach (ZShopBtn btn in listOfShopBtns)
        {
            if(!btn.GetData().IsItemPurchased())
            {
                retVal.Add(btn);
            }
        }
        return retVal;
    }

#if UNITY_EDITOR
    private void LateUpdate()
    {
        GridConstraintFix();
    }
#endif

    void GridConstraintFix()
    {
        //In game constraint fix
        float _rectWidth = Mathf.Clamp (m_ItemsRectBasis.rect.width, m_itemRectLimit.x, m_itemRectLimit.y);

        float _getGridWidth = (_rectWidth / 4) - (m_ItemsGridLayoutGroup.padding.left / 2);
        m_ItemsGridLayoutGroup.cellSize = new Vector2 (_getGridWidth, _getGridWidth);
    }

    public void Reset()
    {
        for(int i = 0; i < ItemCollection.TotalCount(); i++)
        {
            GameItem data = ItemCollection.GetItem(i);
            data.Reset();
        }
    }

    public void Initialize(int p_idx = -1)
    {
        AudioPlayerMgr.Instance.PlaySfx(AudioID.BUTTON_SFX);

        ShopGroup.gameObject.SetActive(true);
        if (!PlayerPrefs.HasKey("ItemSelected"))
        {
            PlayerPrefs.SetInt("ItemSelected", 0);
        }
        else
        {
            curItemSelected = (p_idx >= 0) ? p_idx : PlayerPrefs.GetInt("ItemSelected");
            PlayerPrefs.SetInt("ItemSelected", curItemSelected);
        }

        int idx = 0;
        foreach(ZShopBtn btn in listOfShopBtns)
        {
            GameItem data = ItemCollection.GetItem(idx);
            bool isSelected = (idx == curItemSelected) ? (true) : (false);
            btn.Initialize(data, isSelected);
            idx++;
        }
        WhenInitialized.Invoke();

        if (!isInitialized)
        {
            isInitialized = true;
            LeanTween.cancel(ShopGroup.gameObject);
            LeanTween.moveLocalY(ShopGroup.gameObject, YMax, AnimationSpeed).setEase(LeanTweenType.easeOutSine);
            WhenInitializedOnce.Invoke();
        }

    }

    public void Close()
    {
        AudioPlayerMgr.Instance.PlaySfx(AudioID.BUTTON_SFX);

        LeanTween.cancel(ShopGroup.gameObject);
        LeanTween.moveLocalY(ShopGroup.gameObject, YMin, AnimationSpeed).setEase(LeanTweenType.easeOutSine);

        isInitialized = false;
        WhenDestroyed.Invoke();
    }
}
