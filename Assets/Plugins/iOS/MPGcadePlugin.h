#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

// Root view controller of Unity screen
extern UIViewController *UnityGetGLViewController();

@interface MPGCade : NSObject <SKStoreProductViewControllerDelegate>
- (int)isAppInstalled;
-(void)openAppLocally;
- (void)openAppInStore;
@end
