 //  UnityPluginTest-1.mm
 //  Created by OJ on 7/13/16.
 //  In unity, You'd place this file in your "Assets>plugins>ios" folder

//Objective-C Code
#import "MPGCadePlugin.h"

  @implementation MPGCade

- (id)init
{
    self = [super init];
    
    return self;
}

-(void) openAppLocally: (NSString *)urlScheme
{
    urlScheme = [NSString stringWithFormat:@"MPG%@", urlScheme];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlScheme]];
}

-(void) openAppInStore: (int) appID
{
    if ([SKStoreProductViewController class])
    {
        NSDictionary *parameters = @{SKStoreProductParameterITunesItemIdentifier: [NSNumber numberWithInteger: appID]};

        SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
        [productViewController loadProductWithParameters:parameters completionBlock:nil];
        [productViewController setDelegate:self];
        [UnityGetGLViewController() presentViewController:productViewController animated:YES completion:nil];
    }
//
//    else
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [NSString stringWithFormat: @"http://itunes.apple.com/app/id%d?mt=8", appID]]];
//    }
}

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{   [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    UnitySendMessage("~AppstoreHandler", "appstoreClosed", "");
}

- (BOOL)prefersStatusBarHidden
{   return YES;
}


- (int)isAppInstalled: (NSString *)urlScheme
   {
       int param = 0;
       urlScheme = [NSString stringWithFormat:@"MPG%@", urlScheme];
       if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]])
       {
           param = 1;
       }
       else
       {
           param = 20;
       }

       return param;

    }
  @end

static MPGCade *nativeAppstorePlugin = nil;

 //C-wrapper that Unity communicates with
  extern "C"
  {
     int CheckIfAppInstalled(const char* urlScheme)
     {
         MPGCade *status = [[MPGCade alloc] init];
         NSString* nUrlScheme = [NSString stringWithUTF8String:urlScheme];
         return [status isAppInstalled: nUrlScheme];
     }
      
      void OpenAppLocally(const char* bundleId)
      {
          MPGCade *status = [[MPGCade alloc] init];
          NSString* nUrlScheme = [NSString stringWithUTF8String:bundleId];
          return [status openAppLocally: nUrlScheme];
      }
      
      
      void OpenInAppStore(int bundleId)
      {
          NSLog(@"NativeAppStore :: Open App %d", bundleId);
          
          if (nativeAppstorePlugin == nil)
              nativeAppstorePlugin = [[MPGCade alloc] init];
          
          [nativeAppstorePlugin openAppInStore: bundleId];
      }
}
