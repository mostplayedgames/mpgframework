﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class ShareManager : MonoBehaviour
{
//    private bool TakeScreenShot = false;
//    [SerializeField]
//    private Camera myCam;
//    public static ShareManager Instance;
//    public Text TopText;
//    public Text BottomText;
//    public Image MemeImage;
//    public GameObject ScreenshotCanvas;
//    public GameObject HelpCanvas;
//    public Animator PictureAnim;
//    public string PathFile
//    {
//        get
//        {
//#if UNITY_ANDROID && !UNITY_EDITOR
//            return Application.persistentDataPath + "/Screenshot.png";
//#else
//            return Application.persistentDataPath + "/Screenshot.png";
//#endif
//        }
//    }
//    public ScreenshotLayout layout;

//    private string ToSay;
//    private bool canPicture = true;

//    private void Awake()
//    {
//        if (Instance == null)
//            Instance = this;
//        else if (Instance != this)
//            Destroy(this);

//       // DisableCanvases();
//        myCam = this.GetComponent<Camera>();
//        myCam.enabled = false;
//    }
//    private void Start()
//    {
//        DisableCanvases();
//    }
//    private int current = -1;

//    public void ShareToFriend()
//    {
//        if (!canPicture)
//            return;

//        layout.SetMeme(GTMReader.GetData(GTMGameManager.instance.StageCount - 1));
//        ScreenshotCanvas.SetActive(true);
//        myCam.enabled = true;
//        ToSay = "I AM MEMELORD";
//        ScreenShot(Screen.width, Mathf.RoundToInt((float)Screen.height / 1.5f));
//    }

//    public void AskHelpFromFried()
//    {
//        if (!canPicture)
//            return;

//        HelpCanvas.SetActive(true);
//        HelpCanvas.GetComponent<PuzzleHandler>().FormatMeme(GTMReader.GetData(GTMGameManager.instance.StageCount));
//        myCam.enabled = true;
//        ToSay = "Henlo may i request assistance danya uwu";
//        HelpCanvas.GetComponent<GameLayout>().SetLayout();
//        ScreenShot(Screen.width, Mathf.RoundToInt((float)Screen.height / 1.2f));

//    }

//    //*****************************************************//
//    //**************** For taking pictures ****************//
//    //*****************************************************//

//    public void OnPostRender()
//    {
//        if (!TakeScreenShot)
//            return;

//        TakeScreenShot = false;
//        RenderTexture renderTexture = myCam.targetTexture;

//        Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

//        Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
//        renderResult.ReadPixels(rect, 0, 0);

//        byte[] byteArray = renderResult.EncodeToPNG();
//        System.IO.File.WriteAllBytes(PathFile, byteArray);

//        RenderTexture.ReleaseTemporary(renderTexture);
//        myCam.targetTexture = null;
//        myCam.enabled = false;
//        DisableCanvases();
//        myCam.enabled = false;
//#if UNITY_ANDROID && !UNITY_EDITOR
//        ZNativeShare.Share(ToSay, PathFile,null,ToSay, "image/png");
//#elif UNITY_IOS
//        CallSocialShareAdvanced(ToSay, ToSay, "", PathFile);
//#endif
//    }

//    private void ScreenShot(int w, int h)
//    {
//        myCam.enabled = true;
//        AudioManager.instance.OneShot(7);
//        //ScreenCapture.CaptureScreenshot(Application.dataPath + "/Screenie.png");
//        myCam.targetTexture = RenderTexture.GetTemporary(w, h, 16);
//        StartCoroutine(picture());
//    }

//    IEnumerator picture()
//    {
//        TakeScreenShot = false;

//        PictureAnim.Play("Picture");
//        yield return new WaitForSeconds(1.0f);

//        TakeScreenShot = true;
//    }

//    private void DisableCanvases()
//    {
//        ScreenshotCanvas.SetActive(false);
//        HelpCanvas.SetActive(false);
//    }


//#if UNITY_IOS
//	public struct ConfigStruct
//	{
//		public string title;
//		public string message;
//	}

//	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);
	
//	public struct SocialSharingStruct
//	{
//		public string text;
//		public string url;
//		public string image;
//		public string subject;
//	}
	
//	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);
	
//	public static void CallSocialShare(string title, string message)
//	{
//		ConfigStruct conf = new ConfigStruct();
//		conf.title  = title;
//		conf.message = message;
//		showAlertMessage(ref conf);
//	}

//	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
//	{
//		SocialSharingStruct conf = new SocialSharingStruct();
//		conf.text = defaultTxt; 
//		conf.url = url;
//		conf.image = img;
//		conf.subject = subject;
		
//		showSocialSharing(ref conf);
//	}
//#endif

}
