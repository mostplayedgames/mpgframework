﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MTSTuningMgr : ZSingleton<MTSTuningMgr>
{
    public float GetSpeed(string p_diff)
    {
        switch(p_diff)
        {
            case "E":
                return Random.Range(0.5f, 0.7f);
                break;
            case "M":
                return Random.Range(0.35f, 0.5f);
                break;
            case "H":
                return Random.Range(0.2f, 0.35f);
                break;
            default:
                return Random.Range(0.2f, 0.7f);
                break;
        }
    }

    public float Height(string p_diff)
    {
        switch (p_diff)
        {
            case "E":
                return Random.Range(0f, 1.5f);
                break;
            case "M":
                return Random.Range(2f, 3.5f);
                break;
            case "H":
                return Random.Range(4f, 5f);
                break;
            default:
                return Random.Range(0.2f, 0.7f);
                break;
        }
    }
}
