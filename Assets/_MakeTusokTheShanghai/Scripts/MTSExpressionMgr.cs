﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTSExpressionMgr : ZSingleton<MTSExpressionMgr>
{
    public List<string> listOfExpressions = new List<string>();
    public List<string> gameOverExpressions = new List<string>();
    public List<string> epicExpressions = new List<string>();

    [Space]
    public List<string> weakPerformace = new List<string>();
    public List<string> averagePerformance = new List<string>();
    public List<string> strongPerformance = new List<string>();

    [Space]
    public Color normalTusok;
    public Color specialTusok;
    [Space]
    public Color specialTusokImage;

    [Space]
    [SerializeField] Color BadPerformanceColor;
    [SerializeField] Color GoodPerformanceColor;
    [SerializeField] Color ExcellentPerformanceColor; 

    public Color GetColor(int p_score)
    {
        if(p_score < 10)
        {
            return BadPerformanceColor;
        }
        else if(p_score < 20)
        {
            return GoodPerformanceColor;
        }
        else
        {
            return ExcellentPerformanceColor;
        }

    }

    public string GetWords(int p_score)
    {
        if (p_score < 10)
        {
            int randVal = Random.Range(0, weakPerformace.Count);
            return weakPerformace[randVal];
        }
        else if (p_score < 20)
        {
            int randVal = Random.Range(0, averagePerformance.Count);
            return averagePerformance[randVal];
        }
        else
        {
            int randVal = Random.Range(0, strongPerformance.Count);
            return strongPerformance[randVal];
        }
    }

    public string GetRandomExpression(int p_count)
    {
        int expressionCnt =  Random.Range(0, listOfExpressions.Count);
        string retVal =  listOfExpressions[expressionCnt];

        if(p_count > 1)
        {
            retVal = "" + p_count + "X " + epicExpressions[expressionCnt];
        }

        return retVal;
    }
}
