﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class AxisProperties
{
    public float speed;
    public float originalSpeed;
    public float min;
    public float max;
    public bool goForward;
}

public class MTSFork : ZSingleton<MTSFork>
{
    //[SerializeField] Vector2 speed;
    [SerializeField] SpriteRenderer HandSprite;
    [SerializeField] SpriteRenderer ExtendedHandSprite;
    [SerializeField] Color LockedColor;

    [SerializeField] AxisProperties xAxis;
    [SerializeField] AxisProperties yAxis;
    Vector3 startPos;

    public float period = 5f;

    bool canStab;

    bool didHitSomething;
    MTSForkCollider col;

    public List<string> heightOrder = new List<string>()
    {
        "M","S","H", "S", "H", "M", "H", "S", "M", "S", "H", "S", "H", "M", "S", "H"
    };

    public float MoveSpeed = 5.0f;

    public float frequencey = 20.0f;
    public float magnitude = 0.5f;
    public Vector3 axis;

    private Vector3 pos;

    GameItem currentItem;
    public void SetCharacter(GameItem p_itemData, bool withAnimation)
    {
        currentItem = p_itemData;
        HandSprite.color = Color.white;
        ExtendedHandSprite.color = Color.white;

        if (withAnimation)
        {
            StopCoroutine("SetCharacterAnimation");
            StartCoroutine(SetCharacterAnimation(1));
        }
        else
        {
            HandSprite.sprite = p_itemData.ItemImage;
            ExtendedHandSprite.sprite = p_itemData.ItemImage;
        }
    }

    public void SetCharacter()
    {
        StopCoroutine("SetCharacterAnimation");
        StartCoroutine(SetCharacterAnimation(0));
    }

    public void SetShopPreview(GameItem p_itemData)
    {
        HandSprite.sprite = p_itemData.ItemImage;
        ExtendedHandSprite.sprite = p_itemData.ItemImage;
        HandSprite.color = LockedColor;// Color(0.1f,0.1f, 0.1f, 1f);
        ExtendedHandSprite.color = LockedColor;
    }

    public IEnumerator SetCharacterAnimation(float p_posx)
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalX(this.gameObject, 10, 0.2f);

        yield return new WaitForSeconds(0.2f);
        ZObjectMgr.Instance.ResetAll();
        AudioPlayerMgr.Instance.PlaySfx(AudioID.SWOOSH_SFX);
        this.transform.localScale = new Vector3(this.transform.localScale.x,
                                       Mathf.Abs(this.transform.localScale.y),
                                        this.transform.localScale.z);
        //LeanTween.cancel(this.gameObject);
        if(p_posx == 0)
        {
            MTSTray.Instance.Initialize(true);
        }
        LeanTween.moveLocalX(this.gameObject, p_posx, 0.2f);
        HandSprite.sprite = currentItem.ItemImage;
        ExtendedHandSprite.sprite = currentItem.ItemImage;
    }

    public void SetUnlockPreview()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocal(this.gameObject, new Vector3(0, 0.7f, 0), 0.2f);
        this.transform.localScale = new Vector3(this.transform.localScale.x,
                                       Mathf.Abs(this.transform.localScale.y),
                                        this.transform.localScale.z);
    }

    public void SetShop(bool isShop)
    {
        Vector3 targetVec = (isShop) ? new Vector3(1, 5.5f, 0) : new Vector3(0, 0.7f, 0);
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocal(this.gameObject, targetVec, 0.2f);
    }

    public void QuickScaleUp()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.scale(this.gameObject, this.transform.localScale + new Vector3(0.1f,0.1f, 0.1f), 0.2f).setLoopPingPong().setLoopCount(2);
    }

    public int heightOrderCount;

    private void Awake()
    {
        col = GetComponentInChildren<MTSForkCollider>();
        startPos = transform.position;
        heightOrderCount = Random.Range(0, heightOrder.Count);
        xAxis.speed = 0;
        yAxis.speed = 0;
        axis = transform.right;
        xAxis.originalSpeed = 12;
        yAxis.originalSpeed = 0;
        _frequency = frequencey;

        int itemSelected = PlayerPrefs.GetInt("ItemSelected");
        GameItem item = ItemCollectionMgr.Instance.GetItem(itemSelected);
        SetCharacter(item,false);

        Reset();
    }

    public void Intiailzie()
    {
        didHitSomething = false;
    }

    public void Hit()
    {
        didHitSomething = true;
    }

    public float Speed;

    private IEnumerator MovementLoop()
    {
        //float randSpeed = Random.Range(0, 100) > 80 ? Random.Range(0.6f, 0.8f) : Random.Range(0.8f, 1.5f);
        float randSpeed = Random.Range(0.5f, 1.6f);

        float speed = 1;

       // ChangeSpeed();

        //if(this.transform.localPosition.x < 4 && this.transform.localPosition.x > -4)
        //{
        //    speed = Random.Range(0.5f, 0.8f);
        //}
        Debug.Log("Speed: " + speed);
        int dir = Random.Range(0, 100) > 50 ? 1 : -1;

        float originalSpeed = speed;
        float originalPos = this.transform.localPosition.x;
        float pos = Mathf.Abs(originalPos);
        float ratio = pos / 4.5f;
        bool isSameDir = (Mathf.Sign(dir).Equals(Mathf.Sign(pos)));
        Debug.Log("Dir: " + Mathf.Sign(dir) + "_" + Mathf.Sign(pos));
        ratio += (isSameDir) ? 0 : 0.5f; // if going in the same direction add nothing else if different add 0.5f which is half the value.
        if(pos.Equals(0))
        {
            ratio = 0.5f;
        }

        if(ratio > 0.5f)
        {
            if (!isSameDir)
            {
                float newSpeed = originalSpeed;
                speed *= (ratio);
            }
            else
            {
                float newSpeed = originalSpeed;
                newSpeed *= (ratio);
                speed -= newSpeed;
                Debug.Log("Speed1: " + newSpeed);
            }
            //speed = (speed < 0.5f) ? 0.5f : speed;
        }

        Debug.Log("Ratio: " + ratio + "_" + speed + "_" + originalSpeed + "_" + pos + "_" + isSameDir);

        while (isMovementLooping)
        {
            //if (originalPos >= 4.4f || originalPos <= -4.5f)
            //{
            //    speed = originalSpeed;
            //    Debug.Log("Reset");
            //}

            LeanTween.cancel(this.gameObject);
            LeanTween.moveLocalX(this.gameObject, -4.5f * dir, speed).setEase(LeanTweenType.easeInOutSine);
            yield return new WaitForSeconds(speed);
            speed = originalSpeed;
            LeanTween.moveLocalX(this.gameObject, 4.5f * dir, speed).setEase(LeanTweenType.easeInOutSine);
            yield return new WaitForSeconds(speed);
        }
    }

    bool isMovementLooping;
    public float newSpeed;

    public void ChangeSpeed()
    {
        //frequencey = Random.Range(5, 12);
        tTime = 0;
        int score = ZGameMgr.instance.GetScore();

        // Let's just say that the max speed of the game is 12;

        if(score < 3)
        {
            frequencey = Random.Range(2f, 3f);
        }
        else if (score < 7)
        {
            frequencey = Random.Range(0, 100) > 70 ? Random.Range(3f, 4f) : Random.Range(2f, 3f);
        }
        else if(score < 15)
        {
            frequencey = Random.Range(0, 100) > 70 ? Random.Range(4.5f, 5.5f) : Random.Range(2f, 3.5f);
        }
        else if (score < 24)
        {
            frequencey = Random.Range(0, 100) > 70 ? Random.Range(5f, 6f) : Random.Range(3f, 4.5f);
        }
        else
        {
            frequencey = Random.Range(0, 100) > 50 ? Random.Range(5f, 7f) : Random.Range(1f, 4f);
        }
    }


    public void Reset(bool clearShanghai = true)
    {
        StartCoroutine(ResetSequence(clearShanghai));
    }

    private IEnumerator ResetSequence(bool clearShanghai)
    {
        FakeTime = 0;
        phase = 0;
        incrementingSpeed = 0;
        if(clearShanghai)
        {
            col.Reset();
        }
        xAxis.speed = 0;
        yAxis.speed = 0;
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocal(this.gameObject, new Vector3(0, 0.7f, 0), 0.2f);
        yield return new WaitForSeconds(0.2f);
        //MTSGameManager.Instance.mainMenuGroup.gameObject.SetActive(true);
        canStab = true;
    }

    private IEnumerator StabSequence()
    {
        canStab = false;
        col.hitCount = 0;
        float yPos = this.transform.localPosition.y;



        //LeanTween.moveLocalY(this.gameObject, yPos + 0.5f, 0.05f).setEase(LeanTweenType.easeOutExpo);
        //yield return new WaitForSeconds(0.05f);
        LeanTween.cancel(this.gameObject);
        isMovementLooping = false;
        //StopCoroutine("MovementLoop");
        AudioPlayerMgr.Instance.PlaySfx(AudioID.SWOOSH_SFX);

        LeanTween.moveLocalY(this.gameObject, -1f, 0.1f).setEase(LeanTweenType.easeInOutSine);

        col.gameObject.SetActive(true);

        xAxis.speed = 0;
        yAxis.speed = 0;
        col.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.15f);
        col.gameObject.SetActive(false);


        if (col.hitCount <=0)
        {
            LeanTween.cancel(this.gameObject);
            isMovementLooping = false;
            //StopCoroutine("MovementLoop");
            
            ZGameMgr.instance.GameOver();

            if (Physics2D.OverlapCircle(col.transform.position,10))
            {
                Collider2D[] hitColliders = Physics2D.OverlapCircleAll (col.transform.position, 10.0f);
                
                int i = 0;
                while (i < hitColliders.Length)
                {
                    if (hitColliders[i].GetComponent<Rigidbody2D>())
                    {
                        hitColliders[i].GetComponent<Rigidbody2D>().AddForce(Vector2.up * Random.Range(500, 1000));
                    }
                    i++;
                }
            }
        }
        else
        {
            col.ShowHitEffect();
            LeanTween.cancel(this.gameObject); //Random.Range(2f, 5f)
            LeanTween.moveLocalY(this.gameObject, GetNewHeight(), 0.2f);//.setEase(LeanTweenType.easeOutExpo);
            yield return new WaitForSeconds(0.2f); // 0.2f
            canStab = true;
            xAxis.speed = xAxis.originalSpeed;
            yAxis.speed = yAxis.originalSpeed;
            ChangeSpeed();

           // FakeTime = 0;
            isMovementLooping = true;
            //StopCoroutine("MovementLoop");
            //StartCoroutine("MovementLoop");
        }
        UpdateSpeed();


    }


    public float GetNewHeight()
    {
        int score = ZGameMgr.instance.GetScore();
        string heightTuning = heightOrder[heightOrderCount];
        float retVal = 0;
        Debug.Log("Height: " + heightTuning);

        if(score < 7)
        {
            return Random.Range(2f, 3f);
        }

        switch (heightTuning)
        {
            case "S":
                retVal = (score < 10) ? Random.Range(0f, 1f) : Random.Range(0, 1f);
                break;
            case "M":
                retVal = Random.Range(2f, 3f);
                break;
            case "H":
                retVal = (score < 10) ? Random.Range(4f, 5f) : Random.Range(4, 5f);
                break;
        }

        heightOrderCount = (int)Mathf.Repeat(heightOrderCount + 1, heightOrder.Count);
        return retVal;

    }

    public void UpdateSpeed()
    {
        //incrementingSpeed = 0;
        //int score = ZGameMgr.instance.GetScore();
        //if(score < 6)
        //{
        //    period = Random.Range(0,100) > 20 ? Random.Range(0.4f, 0.5f) : Random.Range(0.3f, 0.35f);
        //}
        //else if(score < 15)
        //{
        //    period = Random.Range(0, 100) > 40 ? Random.Range(0.3f, 0.5f) : Random.Range(0.2f, 0.24f);
        //}
        //else if(score < 20)
        //{
        //    period = Random.Range(0.2f, 0.4f);
        //}
        //else
        //{
        //    period = Random.Range(0.2f, 0.34f);
        //}
    }

    float incrementingSpeed = 0;
    Vector3 _newPosition;
    public float xPos;

    public void Stab()
    {
        StopCoroutine("StabSequence");
        StartCoroutine("StabSequence");
    }


    public float tTime;
    public float FakeTime;
    public float _frequency;
    public float phase;
    public void GameUpdate()
    {

        if(xAxis.speed > 0) 
        {
            FakeTime += Time.deltaTime;
            if (frequencey != _frequency)
                CalcNewFreq();
            //v3.x = Mathf.Sin(Time.time * _frequency + phase) * amplitude;
            Vector3 pos = axis * Mathf.Sin(FakeTime * _frequency + phase) * magnitude;
            transform.position = new Vector3(pos.x, this.transform.localPosition.y, this.transform.localPosition.z);

            //incrementingSpeed += Time.deltaTime;
            //float theta = incrementingSpeed / period;
            //float distance = amplitude * Mathf.Sin(theta);
            //Vector3 movePos = startPos + Vector3.right * distance;
            //transform.position = new Vector3 (movePos.x, this.transform.position.y, this.transform.position.z);
        }
        if (Input.GetKey(KeyCode.A))
        {
            frequencey = Random.Range(1, 10);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            Vector3 pos = axis * Mathf.Sin(FakeTime * _frequency) * magnitude;
            Debug.Log("Sin: " + pos);
        }

        if (Input.GetMouseButtonDown(0) && canStab)
        { 
            Stab();
        }
    }

    void CalcNewFreq()
    {
        Debug.Log("new frequency");
        float curr = (FakeTime * _frequency + phase) % (2.0f * Mathf.PI);
        float next = (FakeTime * frequencey) % (2.0f * Mathf.PI);
        phase = curr - next;
        _frequency = frequencey;
    }
}
