﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LeaderboardData
{
    public string name;
    public string saveName;
    public string leaderboardIOS;
    public string leaderboardAndroid;
}

public class MTSGameManager : ZGameMode//ZSingleton<MTSGameManager>
{
    [Header("Score Related")]
    [SerializeField] int highScore;
    [SerializeField] int totalCount;
    [SerializeField] int targetCount;
    [SerializeField] int score;
    [SerializeField] Text scoreUI;
    [SerializeField] Text bestUI;
    [SerializeField] Text totalScoreUI;

    [SerializeField] GameObject RestartButton;
    [SerializeField] GameObject ResultShareBtn;
    [SerializeField] GameObject ResultTitleBtn;

    [Header("Tray")]
    [SerializeField] MTSTray Tray;

    [Header("Hand")]
    [SerializeField] MTSFork Hand;

    [Header("UI")]
    public GameObject mainMenuGroup;
    public GameObject botMenuGroup;

    [SerializeField] ZLoadingBar scoreLoadingBar;
    [SerializeField] Text gameOverText;

    [SerializeField] List<int> valuesToUnlock = new List<int>();

    public void Awake()
    {
        highScore = PlayerPrefs.GetInt("" + leaderboardData.saveName);
        scoreUI.text = "" + highScore;

        if (!PlayerPrefs.HasKey("TotalCount"))
        {
            PlayerPrefs.SetInt("TotalCount", 0);
        }
        score = 0;
        targetCount = ItemCollectionMgr.Instance.TotalCount();
        Debug.Log("TargetCount: " + targetCount);
        Tray.Initialize(true);

        totalCount = PlayerPrefs.GetInt("TotalCount");
        AddCoins(0);
    }

    public override void ZInitialize()
    {
        bestUI.text = "SCORE";
    }

    public override void ZUpdate()
    {
        MTSFork.Instance.GameUpdate();
    }

    public override int ZGetHighScore()
    {
        return PlayerPrefs.GetInt("" + leaderboardData.saveName);
    }

    public override int ZGetScore()
    {
        return score;
    }

    public override void ZGameOver()
    {
        StopCoroutine("GameOverSequence");
        StartCoroutine("GameOverSequence");
    }

    // Called by a unity invent in the Shop Screen Inspector
    public void OpenShop(bool p_openShop)
    {
        if(p_openShop)
        {
            ZGameMgr.instance.curState = ZGameState.SHOP;
        }
        else
        {
            ZGameMgr.instance.curState = ZGameState.MAIN_MENU;
            ZObjectMgr.Instance.DestroyAll();
            Tray.Initialize(true);
        }
    }

    public void Purchase()
    {
        totalCount -= GetTargetCount();
        PlayerPrefs.SetInt("TotalCount", totalCount);
        targetCount++;
        scoreLoadingBar.UpdateBar(totalCount, GetTargetCount());
    }

    public void ShowTitleScreen(bool p_willShow)
    {
        if(ZGameMgr.instance.curState == ZGameState.SHOP)
        {
            scoreUI.gameObject.SetActive(false);
        }
        else
        {
            scoreUI.gameObject.SetActive(true);
        }

        float targetYPos = (p_willShow) ? 0 : 800;
        LeanTween.cancel(mainMenuGroup.gameObject);
        LeanTween.moveLocalY(mainMenuGroup.gameObject, targetYPos, 0.2f);

        targetYPos = (p_willShow) ? -220 : -400;
        LeanTween.cancel(botMenuGroup.gameObject);
        LeanTween.moveLocalY(botMenuGroup.gameObject, targetYPos, 0.2f);
    }

    public int GetTargetCount()
    {
        return valuesToUnlock[targetCount - 1];
    }


    public int GetTotalCount()
    {
        return totalCount;
    }

    public void UnlockPreview()
    {
        MTSFork.Instance.SetUnlockPreview();
        gameOverText.gameObject.SetActive(false);
        totalScoreUI.gameObject.SetActive(false);
        ZObjectMgr.Instance.ResetAll();
        scoreUI.gameObject.SetActive(false);
        Tray.gameObject.SetActive(false);
        RandomShopUnlockMgr.Instance.FlickerUnlock();
    }

    public IEnumerator GameOverSequence()
    {
        ZCameraMgr.instance.DoShake();
        Tray.speed = 0;
           
        AudioPlayerMgr.Instance.PlaySfx(AudioID.ERROR_SFX);
     
        gameOverText.gameObject.SetActive(true);
        gameOverText.transform.localScale = Vector3.zero;
        gameOverText.text = "" + MTSExpressionMgr.Instance.GetWords(score);
        gameOverText.color = MTSExpressionMgr.Instance.GetColor(score);

        LeanTween.cancel(gameOverText.gameObject);
        LeanTween.scale(gameOverText.gameObject, new Vector3(1, 1, 1), 0.5f).setEase(LeanTweenType.easeOutExpo);

        yield return new WaitForSeconds(2.0f);

        AudioPlayerMgr.Instance.PlaySfx(AudioID.SWOOSH_SFX);
        totalScoreUI.gameObject.SetActive(true);
        totalScoreUI.text = "" + score + " PCS LUMPIA";
        Hand.Reset(false);
        Tray.SlideOut();

        scoreUI.gameObject.SetActive(false);
        Hand.transform.localScale = new Vector3(Hand.transform.localScale.x,
                                               Hand.transform.localScale.y * -1,
                                                Hand.transform.localScale.z);

        if(targetCount < valuesToUnlock.Count)
        {
            if (totalCount >= valuesToUnlock[targetCount - 1])
            {
                RandomShopUnlockMgr.Instance.NewCharacterToUnlock();
            }
            else
            {
                if(ZGameMgr.instance.WillShowRewards())
                {
                    ZGameMgr.instance.ShowRewards();
                    yield return new WaitForSeconds(1);
                }
                AudioPlayerMgr.Instance.PlaySfx(AudioID.SHORT_NOTIF_SFX);
                RestartButton.gameObject.SetActive(true);
                ResultShareBtn.gameObject.SetActive(true);
                ResultTitleBtn.gameObject.SetActive(true);
            }
        }
        else
        {
            AudioPlayerMgr.Instance.PlaySfx(AudioID.SHORT_NOTIF_SFX);
            RestartButton.gameObject.SetActive(true);
            ResultShareBtn.gameObject.SetActive(true);
            ResultTitleBtn.gameObject.SetActive(true);
        }
    }

    public override void ZAddCoins(int p_value)
    {
        AddCoins(p_value);
    }

    public void Restart()
    {
        AudioPlayerMgr.Instance.PlaySfx(AudioID.BUTTON_SFX);
        RestartButton.gameObject.SetActive(false);
        ResultShareBtn.gameObject.SetActive(false);
        ResultTitleBtn.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(false);
        ZObjectMgr.Instance.ResetAll();
        ZScore(0);
        totalScoreUI.gameObject.SetActive(false);
        if(ZGameMgr.instance.curState == ZGameState.SHOP)
        {
            Hand.Reset();
        }
        else
        {
            Hand.SetCharacter();
        }
        Tray.Initialize(true);
        ShowTitleScreen(true);
        ZGameMgr.instance.curState = ZGameState.MAIN_MENU;
        bestUI.text = "BEST";
    }

    public void AddCoins(int p_value)
    {
        totalCount+= p_value;
        PlayerPrefs.SetInt("TotalCount", totalCount);
        scoreLoadingBar.UpdateBar(totalCount, valuesToUnlock[targetCount - 1]);

    }


    public override void ZScore(int p_score)
    {
        AddCoins(p_score);
        // If score is 0 force reset things
        if (p_score != 0)
        {
            score += p_score;
            scoreUI.text = "" + score;

            Tray.shanghaiCount -= (Tray.shanghaiCount > 0)? 1 : 0;

            if (Tray.shanghaiCount <= 0 || score == 1)
            {
                Debug.Log("Iniitialize Tray");
                Tray.Initialize();
            }
            Hand.ChangeSpeed();
        }
        else
        {
            score = 0;
            scoreUI.text = "" + PlayerPrefs.GetInt("" +leaderboardData.saveName);
        }

        if(score > PlayerPrefs.GetInt("" + leaderboardData.saveName))
        {
            PlayerPrefs.SetInt("" + leaderboardData.saveName, score);
        }
    }
}
