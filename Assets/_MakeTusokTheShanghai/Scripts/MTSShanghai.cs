﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTSShanghai : MonoBehaviour
{
    [SerializeField] SpriteRenderer TopLumpia;
    [SerializeField] SpriteRenderer BottomLumpia;
    public bool isCollected;
    public List<Sprite> listOfShanghaiSprites = new List<Sprite>();

    public void Intialize()
    {
        int randVal = Random.Range(0, listOfShanghaiSprites.Count);
        TopLumpia.sortingOrder = 50 - (int)Mathf.Abs((this.transform.position.y * 10f));
        BottomLumpia.sortingOrder = 50 - (int)Mathf.Abs((this.transform.position.y * 10f));
        //this.GetComponent<SpriteRenderer>().sprite = listOfShanghaiSprites[randVal];
        isCollected = false;
    }

    public void Update()
    {
        //if (isCollected) return;
        //Debug.Log("Name: " + this.transform.name + "_" + TopLumpia.sortingOrder + "_" + this.transform.position.y);
        //TopLumpia.sortingOrder = 50 - (int)Mathf.Abs((this.transform.position.y * 10f));
        //BottomLumpia.sortingOrder = 50 - (int)Mathf.Abs((this.transform.position.y * 10f));
    }

    public void Hit()
    {
        isCollected = true;
        MTSTray.Instance.listOfShanghai.Remove(this.gameObject);
        TopLumpia.sortingOrder = 999;
        BottomLumpia.sortingOrder = 1001;
    }
    public void Rotate(int dir)
    {
        //this.GetComponent<Rigidbody2D>().AddTorque(100 * dir);
    }
}
