﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTSTray : ZSingleton<MTSTray>
{
    float minX = -2;
    float maxX = 2;
    bool goLeft;
    public float speed = 7;

    [SerializeField] Collider2D trayCollider;
    [SerializeField] GameObject tray;
    [SerializeField] GameObject TrayParent;
    [SerializeField] List<GameObject> edgeColliders = new List<GameObject>();
    [SerializeField] GameObject SpawnPoint;
    public int shanghaiCount;
    public List<GameObject> listOfShanghai = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
     //   Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        //float curPosX = this.transform.localPosition.x;

        //if (curPosX <= maxX && goLeft)
        //{
        //    curPosX += speed * Time.deltaTime;
        //    if (curPosX >= maxX)
        //    {
        //        goLeft = false;
        //    }
        //}
        //else if (curPosX >= minX && !goLeft)
        //{
        //    curPosX -= speed * Time.deltaTime;
        //    if (curPosX <= minX)
        //    {
        //        goLeft = true;
        //    }
        //}

        //this.transform.localPosition = new Vector3(curPosX, this.transform.localPosition.y, this.transform.localPosition.z);
    }

    public void makeItMove()
    {

    }
    public int GetNumberOfShanghai()
    {
        int score = ZGameMgr.instance.GetScore();
        return Random.Range(1, 5);
        //if(score < 2)
        //{
        //    return Random.Range(1, 4);
        //}
        //else if(score < 15)
        //{
        //    int retVal = Random.Range(0, 100);
        //    return (retVal > 70) ? Random.Range(3, 6): Random.Range(1,4);
        //}
        //else
        //{
        //    int retVal = Random.Range(0, 100); //9 12
        //    return (retVal > 70)? Random.Range(2, 6): Random.Range(1,4);
        //}
    }

    public float GetRandomPos(float boundsX)
    {
        float randVal = Random.Range(0, 100);
        float randXPos = 0;

        //if (randVal < 30)
        //{
        //    randXPos = Random.Range(-boundsX, 0);
        //}
        //else if(randVal < 60)
        //{ðð
        //    randXPos = Random.Range(0, boundsX);
        //}
        //else
        {
            randXPos = Random.Range(-boundsX, boundsX);
        }

        Debug.Log("randXPos: " + randXPos);
        return randXPos;
    }

    public IEnumerator InitializeSequence()
    {
        float score = ZGameMgr.instance.GetScore();

        yield return new WaitForSeconds(0.25f);

        shanghaiCount = 0;
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalX(this.gameObject, -6, 0.25f);
        //foreach (GameObject obj in edgeColliders)
        //{
        //    obj.SetActive(true);
        //}

        yield return new WaitForSeconds(0.25f);

        speed = 7;
        float randSize = 0.25f;
        // For lower levels just spawn normal types of trays
        if(score < 5)
        {
            randSize = 0.2f;
        }
        else if(score < 14)
        {
            randSize = Random.Range(0.2f, 0.3f);
        }
        else if(score < 18)
        {
            randSize = 0.45f; //(Random.Range(0, 100) > 70) ? 0.45f : 0.25f;
        }
        else
        {
            int randX = Random.Range(0, 100);
            if(randX > 40)
            {
                randSize = 0.25f;
            }
            else
            {
                randSize = Random.Range(0.25f, 0.45f);
            }
        }

        Random.Range(0.1f, 0.45f);
        float randPos = Random.Range(-1.5f, 1.5f);

        int numOfShanghaiToSpawn = GetNumberOfShanghai();

        tray.transform.localScale = new Vector3(randSize, tray.transform.localScale.y, tray.transform.localScale.z);
        //tray.transform.localScale = new Vector3(randSize, tray.transform.localScale.y, tray.transform.localScale.z);
        float boundsX = (trayCollider.bounds.center.x + (trayCollider.bounds.extents.x)) * tray.transform.localScale.x;// / 0.55f);
        float boundsY = trayCollider.bounds.extents.y;


        float randXPos = -boundsX;//GetRandomPos(boundsX * tray.transform.localScale.x); //-boundsX - 0.15f; //

        int toSpawn = GetNumberOfShanghaiToSpawn(randSize); //(int)(randSize * 10) * 2;

        Debug.Log("toSpawn: " + toSpawn);
        float xCentering = -(toSpawn - 1) / 2f;

        int stackHeight = GetStackHeight(toSpawn);

        // Create and attach shanghai to parrent tray
        for (int i = 0; i < toSpawn; i++)
        {
            for (int y = 0; y < stackHeight; y++)
            {
                GameObject obj = PooledObjects.Instance.GetObj(PoolObjType.SHANGHAI);

                obj.transform.parent = TrayParent.transform;
                obj.transform.localPosition = new Vector3((i + xCentering) * 0.5f, SpawnPoint.transform.localPosition.y + y, this.transform.localPosition.z);
                obj.transform.eulerAngles = Vector3.zero;

                float randVal = 0.5f;
                obj.transform.localScale = new Vector3(randVal, randVal, randVal);
                obj.GetComponent<Rigidbody2D>().isKinematic = false;
                obj.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                obj.GetComponent<Rigidbody2D>().angularVelocity = 0;
                obj.GetComponent<MTSShanghai>().Intialize();

                randXPos -= 0.6f;//Random.Range(0.5f, 0.7f)

                listOfShanghai.Add(obj);
            }
        }

        if (score > 25)
        {
            // Remove Certain Shanghai in the plate
            float percentage = Random.Range(0.3f, 0.5f);
            int numOfToSpawn = (int)(toSpawn * percentage);
            for (int i = 0; i < numOfToSpawn; i++)
            {
                if (i < listOfShanghai.Count - 1)
                {
                    int data = Random.Range(0, listOfShanghai.Count);
                    listOfShanghai[data].gameObject.SetActive(false);
                }
            }
        }

        // Count all the active shanghai in the game.
        for (int i = 0; i < listOfShanghai.Count; i++)
        {
            if(listOfShanghai[i].gameObject.activeSelf)
            {
                shanghaiCount++;
            }
        }


        this.transform.localPosition = new Vector3(6, this.transform.localPosition.y, this.transform.localPosition.z);
        LeanTween.cancel(this.gameObject);

        float xPos = GetTrayPosition();

        float traySpeed = (score < 15) ? 0.25f : Random.Range(0.1f, 0.25f);

        LeanTween.moveLocalX(this.gameObject, xPos, traySpeed);

        yield return new WaitForSeconds(0.25f);


        foreach(GameObject obj in listOfShanghai)
        {
            obj.transform.parent = null;
            obj.GetComponent<Rigidbody2D>().isKinematic = false;

            //float randVal = 0.5f;// Random.Range(0.4f, 0.6f);
            //obj.transform.localScale= new Vector3(randVal, randVal, randVal);
        }

        //listOfShanghai.Clear();
        //trayComplete = true;
        //StopCoroutine("MovementLoop");
        //StartCoroutine("MovementLoop");
    }

    private float GetTrayPosition()
    {
        int score = ZGameMgr.instance.GetScore();

        if(score < 6)
        {
            return 0;
        }
        else if(score < 10)
        {
            return (Random.Range(0, 100) > 50) ? Random.Range(1.5f, 2f) : Random.Range(-1.5f, -2f);
        }
        else
        {
            int randVal = Random.Range(0, 100);
            if(randVal < 30) // Go Left
            {
                return Random.Range(-1.5f, -2f);
            }
            else if(randVal < 60) // Go Right
            {
                return Random.Range(1.5f, 2f);
            }
            else // Center
            {
                return Random.Range(-0.5f, 0.5f);
            }
        }
    }

    private int GetNumberOfShanghaiToSpawn(float p_trayScale)
    {
        int score = ZGameMgr.instance.GetScore();

        if (p_trayScale < 0.25f)
        {
            Debug.Log("Easyf");
            return Random.Range(1, 4);
        }
        else if (p_trayScale < 0.35f)
        {
            Debug.Log("Med");
            return Random.Range(1, 4);
        }
        else
        {
            Debug.Log("Hard");

            if (score < 18)
            {
                return Random.Range(1, 8);
            }
            else
            {
                return Random.Range(0, 100) > 50 ? Random.Range(1, 4) : Random.Range(6, 8);

            }
        }
    }

    private int GetStackHeight(int p_shanghaiCount)
    {
        int score = ZGameMgr.instance.GetScore();

        if(p_shanghaiCount > 3) // Prevent free farming
        {
            return 1;
        }

        if (score < 6)
        {
            return 1;
        }
        else if (score < 12)
        {
            return (Random.Range(0, 100) > 30) ? 2 : 1;
        }
        else
        {
            return (Random.Range(0, 100) > 90) ? 2 : 1;
        }


    }

    public bool trayComplete;
     
    public void SlideOut()
    {
        foreach (GameObject obj in listOfShanghai)
        {
            obj.transform.parent = TrayParent.transform;
            //float randVal = 0.5f;// Random.Range(0.4f, 0.6f);
            //obj.transform.localScale = new Vector3(randVal, randVal, randVal);
        }
        listOfShanghai.Clear();
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalX(this.gameObject, -10, 0.25f);
    }

    public void StopMovement()
    {
        //trayComplete = false;
        //StopCoroutine("MovementLoop");
        //LeanTween.cancel(this.gameObject);
    }

    private IEnumerator FirstRunSequence()
    {
        float boundsY = trayCollider.bounds.extents.y;

        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalX(this.gameObject, 0, 0.2f);
        GameObject shanghai = PooledObjects.Instance.GetObj(PoolObjType.SHANGHAI);
        shanghai.transform.parent = TrayParent.transform;
        shanghai.transform.localPosition = new Vector3(0, SpawnPoint.transform.localPosition.y, this.transform.localPosition.z);
        //shanghai.transform.localEulerAngles = Vector3.zero;
        shanghai.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        shanghai.GetComponent<Rigidbody2D>().isKinematic = false;
        shanghai.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        shanghai.GetComponent<Rigidbody2D>().angularVelocity = 0;

        //ColorChange.Instance.Reset();

        yield return new WaitForSeconds(0.2f);
        shanghai.transform.parent = null;
       // shanghai.transform.localScale = new Vector3(1f, 1f, 1f);
         
    }

    public void Initialize(bool firstRun = false)
    {
        if(firstRun)
        {
            this.gameObject.SetActive(true);
            //trayComplete = false;
            //StopCoroutine("MovementLoop");
            StopCoroutine("FirstRunSequence");
            StartCoroutine("FirstRunSequence");
        }
        else
        {
            StopCoroutine("InitializeSequence");
            StartCoroutine("InitializeSequence");
        }

    }
}
