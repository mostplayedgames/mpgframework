﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SFBehaviour
{
    VERTICAL,
    THRUST
}
public class MTSSideFork : MonoBehaviour
{
    public SFBehaviour curBehaviour;
    [SerializeField] GameObject arm;
    [SerializeField] MTSForkCollider forkCollider;
    [SerializeField] float minY;
    [SerializeField] float maxY;

    [SerializeField] float rotSpeed;
    [SerializeField] float thrustSpeed;
    bool goUp;
    bool thrustForward = true;

    [SerializeField] float curRot;
    [SerializeField] float curPos;
    [SerializeField] Vector3 startVec;


    // Start is called before the first frame update
    void Start()
    {
        curBehaviour = SFBehaviour.VERTICAL;
    }

    // Update is called once per frame
    void Update()
    {
        switch(curBehaviour)
        {
            case SFBehaviour.VERTICAL:
                if (curRot <= maxY && goUp)
                {
                    curRot += rotSpeed * Time.deltaTime;
                    if (curRot >= maxY)
                    {
                        goUp = false;
                    }
                }
                else if (curRot >= minY && !goUp)
                {
                    curRot -= rotSpeed * Time.deltaTime;
                    if (curRot <= minY)
                    {
                        goUp = true;
                    }
                }
                this.transform.position = new Vector3(this.transform.position.x, curRot, this.transform.position.z);

                break;
            case SFBehaviour.THRUST:
                if (thrustForward)
                {
                    //if (arm.transform.parent == null)
                    //{
                    //    arm.transform.parent = this.transform;
                    //}

                    if (this.transform.position.y > -3)
                    {
                        this.transform.position += -this.transform.up * thrustSpeed * Time.deltaTime;
                    }
                    else
                    {
                        thrustForward = false;
                    }
                }
                else
                {
                    if (this.transform.position.y < 3)
                    {
                        this.transform.position += this.transform.up * thrustSpeed * Time.deltaTime;
                    }
                    else
                    {
                        //arm.transform.parent = null;
                        //arm.transform.position = this.transform.position;
                        this.transform.position = startVec;
                        forkCollider.hitCount = 0;
                        curBehaviour = SFBehaviour.VERTICAL;
                        thrustForward = true;
                    }
                }
                break;
        }

        if(Input.GetMouseButtonDown(0))
        {
            startVec = this.transform.position;
            curBehaviour = SFBehaviour.THRUST;
        }

    }
}
