﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MTSForkCollider : MonoBehaviour
{
    [SerializeField] GameObject holder;
    MTSFork fork;
    public int hitCount;
    Vector3 lastPosHit;

    public void Reset()
    {
        foreach(Transform obj in holder.transform)
        {
            obj.gameObject.SetActive(false);
            obj.parent = null;
        }
    }

    public void ShanghaiHit(GameObject other)
    {
        ZGameMgr.instance.Score(1);
       
        lastPosHit = other.transform.position;
        // SFX
        AudioPlayerMgr.Instance.PlaySfx(AudioID.SHANGHAI_SFX);

        // SHANGHAI ATTACHEMENT
        GameObject shanghai = other.gameObject;
        shanghai.transform.parent = holder.transform;
        shanghai.GetComponent<Rigidbody2D>().isKinematic = true;
        shanghai.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        shanghai.GetComponent<Rigidbody2D>().angularVelocity = 0;
        Vector3 newPos = new Vector3(Random.Range(-0.25f, 0.25f), Random.Range(0, -0.5f), 0);
        LeanTween.cancel(shanghai.gameObject);
        LeanTween.moveLocal(shanghai.gameObject, newPos, 0.1f);
        //shanghai.transform.localPosition = newPos;
        shanghai.transform.eulerAngles = Vector3.zero;
        shanghai.GetComponent<MTSShanghai>().Hit();
        shanghai.GetComponent<MTSShanghai>().isCollected = true;

        // SHANGHAI PARTICLES
        GameObject obj = PooledObjects.Instance.GetObj(PoolObjType.SHANGHAI_PARTICLES);
        obj.transform.position = shanghai.transform.position - new Vector3(0,1,0);

        hitCount += 1;
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.GetComponent<MTSShanghai>())
        {
            ShanghaiHit(other.gameObject);
            //fork.isMovementLooping = false;
            //StopCoroutine("DelayedEffect");
            //StartCoroutine("DelayedEffect");

            //shanghaiHit = true;
            if (hitCount > 1)
            {
                ZCameraMgr.instance.VerticalShake(0.1f);
            }
        }
    }


    public void ShowHitEffect()
    {
        // HIT UI EFFECT
        MTSTextExpression ui = PooledObjects.Instance.GetObj(PoolObjType.UI_EXPRESSION).GetComponent<MTSTextExpression>();
        ui.transform.position = this.transform.position + new Vector3(0,1f,0);
        ui.Activate(hitCount);


    }

}
