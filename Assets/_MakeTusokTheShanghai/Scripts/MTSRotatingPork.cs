﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ForkBehaviour
{
    ROTATE,
    THRUST,
    GAMEOVER,
}

public class MTSRotatingPork : MonoBehaviour
{
    [SerializeField] GameObject arm;
    [SerializeField] MTSForkCollider forkCollider;
    [SerializeField] float minRot;
    [SerializeField] float maxRot;

    [SerializeField] float rotSpeed;
    [SerializeField] float thrustSpeed;
    public ForkBehaviour curBehaviour;
    bool goLeft;
    bool thrustForward = true;

    [SerializeField] float curRot;
    [SerializeField] float curPos;
    [SerializeField] Vector3 startVec;
    // Start is called before the first frame update
    void Start()
    {
        goLeft = true;
        curBehaviour = ForkBehaviour.ROTATE;
        startVec = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        switch(curBehaviour)
        {
            case ForkBehaviour.ROTATE:
                if(curRot <= maxRot && goLeft)
                {
                    curRot += rotSpeed * Time.deltaTime;
                    if(curRot >= maxRot)
                    {
                        goLeft = false;
                    }
                }
                else if(curRot >= minRot && !goLeft)
                {
                    curRot -= rotSpeed * Time.deltaTime;
                    if(curRot <= minRot)
                    {
                        goLeft = true;
                    }
                }
                break;
            case ForkBehaviour.THRUST:
               if(thrustForward)
                {
                    if(arm.transform.parent == null)
                    {
                        arm.transform.parent = this.transform;
                    }

                    if (this.transform.position.y > -3)
                    {
                        this.transform.position += -this.transform.up * thrustSpeed * Time.deltaTime;
                    }
                    else
                    {
                        thrustForward = false;
                    }
                }
                else
                {
                    if (this.transform.position.y < 3)
                    {
                        this.transform.position += this.transform.up * thrustSpeed * Time.deltaTime;
                    }
                    else
                    {
                        arm.transform.parent = null;
                        arm.transform.position = this.transform.position;
                        this.transform.position = startVec;
                        forkCollider.hitCount = 0;
                        curBehaviour = ForkBehaviour.ROTATE;
                        thrustForward = true;
                    }
                }
                break;
            case ForkBehaviour.GAMEOVER:
                break;
        }

        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("ButtonDown!");
            curBehaviour = ForkBehaviour.THRUST;
        }

        this.transform.localEulerAngles = new Vector3(0, 0, curRot);
    }
}
