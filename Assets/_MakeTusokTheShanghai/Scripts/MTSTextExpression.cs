﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MTSTextExpression : MonoBehaviour
{
    Vector3 startVec = new Vector3(0,0, 0);
    Vector3 targetVec = new Vector3(0.7f, 0.7f, 0.7f);
    float speed = 0.25f;

    public void Activate(int p_hitCount)
    {
        float randRot = Random.Range(-35, 35);
       //this.transform.localEulerAngles = new Vector3(this.transform.localRotation.x, this.transform.localRotation.y, randRot);
        this.transform.localScale = startVec;
        this.GetComponentInChildren<Text>().text = MTSExpressionMgr.Instance.GetRandomExpression(p_hitCount);

        if (p_hitCount > 1)
        {
            //this.GetComponentInChildren<Text>().text = "+" + p_hitCount;
            this.GetComponent<Image>().color = MTSExpressionMgr.Instance.specialTusokImage;
             this.GetComponentInChildren<Text>().color = Color.white;// MTSExpressionMgr.Instance.specialTusok;
            targetVec = new Vector3(0.7f, 0.7f, 0.7f);
            AudioPlayerMgr.Instance.PlaySfx(AudioID.BONUS_SFX);
        }
        else
        {
            //this.GetComponentInChildren<Text>().text = "+1";
            this.GetComponent<Image>().color = Color.white;
            this.GetComponentInChildren<Text>().color = MTSExpressionMgr.Instance.normalTusok;
            targetVec = new Vector3(0.5f, 0.5f, 0.5f);
        }

        //this.transform.position = new Vector3(0, 5, 0);

        StopCoroutine("AnimationSequence");
        StartCoroutine("AnimationSequence");

    }
     
    public IEnumerator AnimationSequence()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.scale(this.gameObject, targetVec, speed * 2).setEase(LeanTweenType.easeOutExpo);
        //yield return new WaitForSeconds(speed);
        int dir = (MTSFork.Instance.transform.position.x > 0) ? -1: 1;
        LeanTween.moveLocal(this.gameObject, this.transform.localPosition + new Vector3(0 * dir, 50, 0), speed * 2).setEase(LeanTweenType.easeOutExpo);

        yield return new WaitForSeconds(speed * 2);
        LeanTween.scale(this.gameObject, Vector3.zero, speed/2).setEase(LeanTweenType.easeOutExpo);
        yield return new WaitForSeconds(speed / 2);
        this.gameObject.SetActive(false);
    }
}
