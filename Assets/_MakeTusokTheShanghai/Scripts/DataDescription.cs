﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataDescription : MonoBehaviour
{
    [SerializeField] Text itemName;
    [SerializeField] Text itemDesc;

    public void Initialize()
    {
        itemName.gameObject.SetActive(true);
        itemDesc.gameObject.SetActive(true);

        int id = ShopManager.Instance.curItemSelected;
        GameItem data = ItemCollectionMgr.Instance.GetItem(id);

        itemName.text = "" + data.ItemName;
        string dataToPrint = data.ItemDescription.Replace(":", "\n");
        itemDesc.text = "" + dataToPrint;
    }

    public void DeActivate()
    {
        itemName.gameObject.SetActive(false);
        itemDesc.gameObject.SetActive(false);
    }
}
