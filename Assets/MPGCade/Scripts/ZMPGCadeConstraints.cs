﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ZMPGCadeConstraints : MonoBehaviour
{
    [SerializeField]
    RectTransform m_GamesListMainRect;

    [Header("Featured Game")]
    [SerializeField]
    LayoutElement m_FeaturedImageLayoutElement;

    [SerializeField]
    float m_ReferenceFeaturedGameOffset;

    [Header("Games Grid")]
    [SerializeField]
    GridLayoutGroup m_GamesGridLayoutGroup;

    [SerializeField]
    float m_RefGridElementHeight;

    void Start()
    {
        FixConstraints();
        Canvas.ForceUpdateCanvases();
    }

    void LateUpdate()
    {
        // #if UNITY_EDITOR
        FixConstraints();
        // #endif
    }

    void FixConstraints()
    {
        try
        {
            float _getGameListWidth = m_GamesListMainRect.rect.width;
            m_FeaturedImageLayoutElement.preferredHeight = (_getGameListWidth / 2) + m_ReferenceFeaturedGameOffset;

            float _getElementWidth = (_getGameListWidth / 2) - (m_GamesGridLayoutGroup.spacing.x / 2);
            m_GamesGridLayoutGroup.cellSize = new Vector2(_getElementWidth, (_getElementWidth / 2) + m_RefGridElementHeight);
        }
        catch
        {

        }
    }
}
