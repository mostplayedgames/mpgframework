﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UIGridHorizontalRectFix : MonoBehaviour
{
    [SerializeField]
	[Tooltip("Get the content rect of its container.")]
    RectTransform m_RectBasis;

	[SerializeField]
	RectTransform m_RectTransform;

    [SerializeField]
	[Tooltip("X = Min; Y = Max")]
    Vector2 m_rectWidthLimit;

	[SerializeField]
	GridLayoutGroup m_GridLayoutGroup;

	[SerializeField]
	int m_FixedElementCount;

	[Header("For not equal layouts")]	
	[SerializeField]
	float m_HeightReference;

	void Awake()
	{
		GridHorizontalRectFix();
		Canvas.ForceUpdateCanvases();
	}

	#if UNITY_EDITOR
	void LateUpdate()
	{
		GridHorizontalRectFix();
	}
	#endif

	void GridHorizontalRectFix()
	{
		if (/* UnityEditor.EditorApplication.isPlaying ||*/ m_GridLayoutGroup == null || m_RectTransform == null) 
			return;

        float _rectWidth = Mathf.Clamp(m_RectBasis.rect.width, m_rectWidthLimit.x, m_rectWidthLimit.y);
        float _getGridWidth = (_rectWidth / m_FixedElementCount) - (m_GridLayoutGroup.padding.left / 2);
		m_GridLayoutGroup.cellSize = 
		new Vector2(_getGridWidth, m_HeightReference > 0 ? m_HeightReference : _getGridWidth);
	}
}