﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MPGCadeBtn : MonoBehaviour 
{
    MPGcadeData data;

    [SerializeField]
    MPGCadeBtnTag gameTag;

    [SerializeField]
    Button m_ButtonComponent;

    [SerializeField]
    Image m_ImageComponent;

    public void Initialize(MPGcadeData p_data)
    {
        // If the game is coming soon grey out the game. 
        if (p_data.Tag == 3)
        {
            m_ButtonComponent.enabled = false;
            m_ImageComponent.color = new Color(0.5f, 0.5f, 0.5f);
        }

        data = p_data;
        m_ImageComponent.sprite = Resources.Load<Sprite>("MPGCade/Icons/" + data.GameName) as Sprite;
        RefreshGameButtonData();
    }

    public void RefreshGameButtonData()
    {
        bool isTagActive = (!data.Tag.Equals("")) ? true : false;
        isTagActive = (data.isInstalled) ? false : true;

        gameTag.gameObject.SetActive(isTagActive);

        if (isTagActive && (data.Tag > 0)) gameTag.Initialize(data.Tag);
    }

    public void ButtonPressed()
    {
        ZMPGCade.Instance.LoadApp(data);
    }
}
