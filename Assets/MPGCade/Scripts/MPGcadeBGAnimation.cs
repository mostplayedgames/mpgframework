﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPGcadeBGAnimation : MonoBehaviour
{
    Material gameMaterial;
    [SerializeField] float scrollingSpeed = 2.0f;
    [SerializeField] Vector2 direction;

    [SerializeField]
    Image m_ImageComponent;

    void Start()
    {
        gameMaterial = new Material(m_ImageComponent.material) { mainTextureOffset = Vector3.zero };
        m_ImageComponent.material = gameMaterial;
    }

    void Update()
    {
        if (gameMaterial != null)
        {
            //scrollingSpeed += Time.deltaTime;
            gameMaterial.mainTextureOffset += new Vector2(scrollingSpeed * direction.x, scrollingSpeed * direction.y) * Time.deltaTime;
        }    
    }
}
