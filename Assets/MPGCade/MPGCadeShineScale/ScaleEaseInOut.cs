﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleEaseInOut : MonoBehaviour 
{
	float m_currentTime;
	bool m_toInverse;

	[SerializeField]
	Vector2 m_RandomAmplitude;
	float m_Amplitude;

	[SerializeField]
	Vector2 m_RandomMagnitude;
	float m_Magnitude;

	float m_NormalScale;
	bool m_isInit;

	[SerializeField]
	bool m_onlyPositive;

	void OnEnable()
	{
		if (!m_isInit)
		{
			m_NormalScale = transform.localScale.x;
			m_isInit = true;
		}

		// LeanTween.cancel(gameObject);
		transform.localScale = new Vector3(m_NormalScale, m_NormalScale, 1);
		m_currentTime = Random.Range(-10f, 10f);
		m_toInverse = Random.Range(0f, 100f) > 50;

		//Randomize 
		m_Amplitude = Random.Range(m_RandomAmplitude.x, m_RandomAmplitude.y);
		m_Magnitude = Random.Range(m_RandomMagnitude.x, m_RandomMagnitude.y);

		RefreshSineWave();
	}

	void Update()
	{
		m_currentTime += Time.unscaledDeltaTime * (m_toInverse ? -1 : 1);
		RefreshSineWave();
	}

	void RefreshSineWave()
	{
		float _modifiedSineWaveValue = (Mathf.Sin(m_currentTime * m_Amplitude) * m_Magnitude);
		float _setSineValue = m_NormalScale + (m_onlyPositive ? Mathf.Abs(_modifiedSineWaveValue) : _modifiedSineWaveValue);
		transform.localScale = new Vector3(_setSineValue, _setSineValue, 1);
	}
}
